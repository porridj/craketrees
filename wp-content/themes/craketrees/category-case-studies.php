<?php get_header(); ?>
			
	<div id="content" class="bg-white">

		<div class="row pm">
		<header class="columns large-10 large-centered text-center">
    		<h1 class="page-title">Case studies</h1>
			<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
    	</header>
		</div>	
	
		<div class="bg-darkgrey pm">
	
		  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		 
				<?php get_template_part( 'parts/loop', 'archive-case-study' ); ?>
			    
			<?php endwhile; ?>	

				<?php joints_page_navi(); ?>
				
			<?php else : ?>
										
				<?php get_template_part( 'parts/content', 'missing' ); ?>
					
			<?php endif; ?>

		</div>

	</div> <!-- end #content -->

<?php get_footer(); ?>
