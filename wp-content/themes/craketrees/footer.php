<?php $facebook 	= get_field('facebook', 'option'); ?>
<?php $twitter 		= get_field('twitter', 'option'); ?>
<?php $instagram 	= get_field('instagram', 'option'); ?>
<?php $tripadvisor 	= get_field('tripadvisor', 'option'); ?>
					
		<footer class=" footer bg-darkgrey pm " role="contentinfo">
			<div class="row ">
				<div class="small-12 columns mbm text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/craketreesmanor-logo-white.svg" title="<?php bloginfo('name'); ?>" width="150">
					
				</div>
			</div>

		
			<div class="row ">
				<div class="small-12 large-4 columns mbm text-center">
					<h6 class="mz">Call us: <a href="tel:+441931715205">01931 715205</a></h6>
				</div>
				<div class="small-12 large-4 columns mbm text-center">
				<?php if ( $facebook || $twitter || $instagram || $tripadvisor ) : ?>

					<div class="social light-links">
						<?php if ( $facebook ) : ?><a href="<?php echo $facebook; ?>" target="_blank" class=""><i class="fab fa-facebook fa-fw"></i></a><?php endif; ?>
						<?php if ( $tripadvisor ) : ?><a href="<?php echo $tripadvisor; ?>" target="_blank" class=""><i class="fab fa-tripadvisor fa-fw"></i></a><?php endif; ?>
						<?php if ( $twitter ) : ?><a href="<?php echo $twitter; ?>" target="_blank" class=""><i class="fab fa-twitter fa-fw"></i></a><?php endif; ?>
						<?php if ( $instagram ) : ?><a href="<?php echo $instagram; ?>" target="_blank" class=""><i class="fab fa-instagram fa-fw"></i></a><?php endif; ?>
					</div>
				<?php endif; ?>
				</div>
				<div class="small-12 large-4 columns mbm text-center">
						<h6 class="mz"><a href="<?php the_permalink(12); ?>">Book online</a></h6>
				</div>
			</div>

			<div class="row ">
				<div class="small-12 large-4 columns mbs text-center">
					<?php if ( is_active_sidebar( 'footer1' ) ) : ?>
							<?php dynamic_sidebar( 'footer1' ); ?>
					<?php endif; ?>
				</div>
				<div class="small-12 large-4 columns mbs">
					<?php if ( is_active_sidebar( 'footer2' ) ) : ?>
						<?php dynamic_sidebar( 'footer2' ); ?>
					<?php endif; ?>
					<div class="row">
						<div class="column small-6 text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/eafrd.png">
						</div>
						<div class="column small-6 text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_leader.png" width="100">
						</div>
						<div class="column text-center">
							
						<p style="line-height: 1;"><small>Redevelopment of bed and breakfast facilities was part funded by the European Agricultural Fund for Rural Development.</small></p>
						</div>
					</div>
				</div>
				<div class="small-12 large-4 columns mbs">
					<?php if ( is_active_sidebar( 'footer3' ) ) : ?>
						<?php dynamic_sidebar( 'footer3' ); ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="row">
				<div class="text-center columns">
					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. <?php echo wp_strip_all_tags(get_field('address_full', 'options'), true); ?></p>
					
					<?php joints_footer_links(); ?>	

				</div>
			</div>
			
		</footer> <!-- end .footer -->
		

		<?php wp_footer(); ?>
		<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"5238a9519c9d9304d68156142","lid":"f1ccb92e12","uniqueMethods":true}) })</script>
	</body>
</html> <!-- end page -->