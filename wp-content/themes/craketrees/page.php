<?php get_header(); ?>


<?php if(is_front_page() && 2 == 3 ) : ?>
	<?php get_template_part( 'parts/video-banner' ); ?>
<?php else: ?>
<?php get_template_part( 'parts/loop', 'banner' ); ?>
<?php endif; ?>

<div id="content">
		

	<?php $hide_title_etc = get_field('hide_title_and_opening_text'); ?>
	<?php if( !$hide_title_etc ) : ?>
			
		<div id="inner-content" class="row">
		    <main id="main" class="small-12 medium-10 medium-centered large-8 columns" role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    <?php endwhile; endif; ?>									
			</main> <!-- end #main -->
		</div> <!-- end #inner-content -->

	<?php endif; ?>

	<?php get_template_part( 'parts/loop', 'flexible_content' ); ?>
	
</div>
<?php get_footer(); ?>