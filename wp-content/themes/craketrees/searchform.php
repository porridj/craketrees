<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
<div class="input-group">

  <input class="input-group-field" type="search" placeholder="<?php echo esc_attr_x( 'Search...', 'jointswp' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>" />
  <div class="input-group-button">
  <button type="submit" class="button" value="Submit"><i class="fa fa-search" aria-hidden="true"></i></button>


  </div>
</div>
</form>