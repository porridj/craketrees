<?php get_header(); ?>


<?php if(is_front_page()) : ?>
	<?php get_template_part( 'parts/video-banner' ); ?>
<?php else: ?>
<?php get_template_part( 'parts/loop', 'banner' ); ?>
<?php endif; ?>

<div id="content">
		

	<?php $hide_title_etc = get_field('hide_title_and_opening_text'); ?>
	<?php if( !$hide_title_etc ) : ?>
			
		<div id="inner-content" class="row">
		    <main id="main" class="small-12 medium-10 medium-centered large-8 columns" role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    	<article id="post-<?php the_ID(); ?>" <?php post_class('mm'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
											

						<header class="article-header ps">
							<h1 class="page-title text-center"><?php the_title(); ?></h1>
					<?php //if ( function_exists('yoast_breadcrumb') && !is_front_page() && !is_page('stay-with-us') ) { yoast_breadcrumb('<div id="breadcrumbs" class="text-center">','</div>'); } ?>
						</header> <!-- end article header -->
					    <section class="entry-content" itemprop="articleBody">
						    <?php the_content(); ?>
						    <?php wp_link_pages(); ?>
						</section> <!-- end article section -->
											
						<footer class="article-footer">
							
						</footer> <!-- end article footer -->
											    
						<?php comments_template(); ?>
										
					</article> <!-- end article -->
			    <?php endwhile; endif; ?>									
			</main> <!-- end #main -->
		</div> <!-- end #inner-content -->

	<?php endif; ?>

	<?php get_template_part( 'parts/loop', 'flexible_content' ); ?>
	
</div>
<?php get_footer(); ?>