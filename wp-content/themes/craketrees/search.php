<?php get_header(); ?>
			
	<div id="content">

		<div id="inner-content" class="row pl">
	
			<main id="main" class="large-8 medium-8 columns medium-centered first" role="main">
				<header>
					<h1 class="archive-title text-center"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>
				</header>
				<div class="row mbs">
				    	<div class="columns small-12 ">
							<?php $categories = get_terms('category'); ?>
							<?php if ( ! empty( $categories ) && ! is_wp_error( $categories ) ) {
							    $count = count( $categories );
							    $i = 0;
							    $term_list = '<div class="block-menu menu-centered"><ul class="vertical medium-horizontal menu dropdown">';
							    foreach ( $categories as $term ) {
							        $i++;
							        $term_list .= '<li><a href="' . esc_url( get_term_link( $term ) ) . '" alt="' . esc_attr( sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) ) . '">' . $term->name . '</a></li>';
							    }
					            $term_list .= '</ul></div>';
							    echo $term_list;
							} ?>
				    	</div><!-- /.columns -->
				    </div><!-- /.row -->

				    <div class="row">
				    	<div class="columns small-12 ">
						    <?php get_search_form(); ?>
				    	</div><!-- /.columns -->
			    	</div>
		    	</header>
				    
				 	
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 

					<?php get_template_part( 'parts/loop', 'archive' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
				
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
			    <?php endif; ?>
	
		    </main> <!-- end #main -->
		
		    <?php //get_sidebar(); ?>
		
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
