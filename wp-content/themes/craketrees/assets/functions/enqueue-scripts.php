<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way    

    // Add vendor scripts 
    wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/assets/js/vendor.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'lightbox-js', get_template_directory_uri() . '/vendor/lightgallery/dist/js/lightgallery-all.js', array( 'jquery' ), '', true );
    // wp_enqueue_script( 'lightboxfull-js', get_template_directory_uri() . '/vendor/lg-fullscreen/dist/lg-fullscreen.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'fontawesome-js', get_template_directory_uri() . '/assets/js/fontawesome-all.min.js', array( 'jquery' ), '', true );

    // Adding scripts file in the footer
    wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/js/parallax.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.min.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'google-map-api', '//maps.googleapis.com/maps/api/js?key=AIzaSyAmAf0e0aHLrfcTy7bSlm35CnMlfZNHJMc', array(), '3', true );
   
    // Register bower stuff
    // wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/vendor/font-awesome/css/font-awesome.min.css', array(), '', 'all' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);