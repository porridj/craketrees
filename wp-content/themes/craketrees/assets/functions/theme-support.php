<?php
	
// Adding WP Functions & Theme Support
function joints_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );
	
	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);
	add_image_size( 'banner-large', 1600, 700, true );
	add_image_size( 'banner-medium', 1024, 256, true );
	add_image_size( 'banner-small', 800, 500, true );
	add_image_size( 'gallery-thumbnail', 250, 250, true );
	add_image_size( 'swipebox', 1200, 1200, false );
	add_image_size( 'square', 600, 600, true );



	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );
	
	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );
	
	// Add HTML5 Support
	add_theme_support( 'html5', 
	         array( 
	         	'comment-list', 
	         	'comment-form', 
	         	'search-form', 
	         ) 
	);
	
	// Adding post format support
	 // add_theme_support( 'post-formats',
		// array(
			// 'aside',             // title less blurb
			// 'gallery',           // gallery of images
			// 'link',              // quick link to other site
			// 'image',             // an image
			// 'quote',             // a quick quote
			// 'status',            // a Facebook like status update
			// 'video',             // video
			// 'audio',             // audio
			// 'chat'               // chat transcript
		// )
	// ); 
	
	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters( 'joints_theme_support', 1200 );	
	
} /* end theme support */

add_action( 'after_setup_theme', 'joints_theme_support' );

function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

// allows you to set custom excerpt lengths
function excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}

// allows you to set custom content length
function content($limit) {
    return wp_trim_words(get_the_content(), $limit);
}

add_filter( 'wpseo_breadcrumb_links', 'override_yoast_breadcrumb_trail' );

function override_yoast_breadcrumb_trail( $links ) {

    global $post;

    // if ( is_singular( 'event' ) ) {
    //     $breadcrumb[] = array(
    //         'url' => get_permalink( get_id_by_slug( 'events' ) ),
    //         'text' => 'Events',
    //     );

    //     array_splice( $links, 1, -2, $breadcrumb );
    // }

    //  if ( is_singular( 'lodge' ) ) {
    //     $breadcrumb[] = array(
    //         'url' => get_permalink( get_id_by_slug( 'lodges' ) ),
    //         'text' => 'Lodges',
    //     );

    //     array_splice( $links, 1, -2, $breadcrumb );
    // }

    return $links;
}

/* GammaFX: Preventing Hidden WooCommerce products from showing up in WordPress search results */
if ( ! function_exists( 'gamma_search_query_fix' ) ){
  function gamma_search_query_fix( $query = false ) {
    if(!is_admin() && is_search()){
      $query->set( 'meta_query', array(
        'relation' => 'OR',
        array(
          'key' => '_visibility',
          'value' => 'hidden',
          'compare' => 'NOT EXISTS',
        ),
        array(
          'key' => '_visibility',
          'value' => 'hidden',
          'compare' => '!=',
        ),
      ));
    }
  }
}
add_action( 'pre_get_posts', 'gamma_search_query_fix' );