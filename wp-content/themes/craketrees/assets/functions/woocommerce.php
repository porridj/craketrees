<?php

// WooCommerce remove "company name" field from Woo Checkout Page
add_filter ( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
  function custom_override_default_address_fields( $address_fields ) {
  unset($address_fields['company']);
  $address_fields['address_2']['placeholder'] = '';

  return $address_fields;
}

// // Display 24 products per page. Goes in functions.php
// add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 30;' ), 20 );

function mytheme_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_template_single_title', 5 );




// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
// add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_add_to_cart', 20 );

// add_filter('frm_redirect_url', 'return_page', 9, 3);
// function return_page($url, $form, $params){
//   if($form->id == 3){ //change 5 to the ID of the form to redirect
//     $field_id = 74; //change 125 the the ID of the radio or dropdown field

//     $url = '/thanks-more-info?download_id=' . $_POST['item_meta'][$field_id];
//   }
//   return $url;
// }

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {


    // unset( $tabs['description'] );         // Remove the description tab
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab

    return $tabs;

}

