<?php 
/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */
class awards_Widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     **/
    function awards_Widget() {
        $widget_ops = array( 'classname' => 'awards-widget', 'description' => 'A gallery of award images set in the Site Settings admin page' );
        $this->WP_Widget( 'awards-widget', 'Awards', $widget_ops );
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        echo $before_widget;


		$awards_array = get_field('awards', 'options');

		if( $awards_array ): $i = 0; ?>
		<div class=" ">
			
			<div class="row text-center brand-logo-gallery">

					<?php foreach( $awards_array as $award ): $i++; ?>
								<div class="va-mid inline">
									<img src="<?php echo $award['sizes']['logo']; ?>" width="<?php echo $award['sizes']['logo-width']/3; ?>"  height="<?php echo $award['sizes']['logo-height']/3; ?>" alt="<?php echo $award['alt']; ?>" class="logo pas" />
								</div>
					<?php endforeach; ?>

			</div>

		</div>
		<?php endif;

    echo $after_widget;
    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    function update( $new_instance, $old_instance ) {

        // update logic goes here
        $updated_instance = $new_instance;
        return $updated_instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void Echoes it's output
     **/
    function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );

        // display field names here using:
        // $this->get_field_id( 'option_name' ) - the CSS ID
        // $this->get_field_name( 'option_name' ) - the HTML name
        // $instance['option_name'] - the option value
    }
}

add_action( 'widgets_init', create_function( '', "register_widget( 'awards_Widget' );" ) );	
