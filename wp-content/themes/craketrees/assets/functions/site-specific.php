<?php 

function footer_menu_item_class( $classes, $item, $args ) {
    // Only affect the menu placed in the 'secondary' wp_nav_bar() theme location
    if ( 'footer-links' === $args->theme_location ) {
        // Make these items 3-columns wide in Bootstrap
        // $classes[] = 'border-bottom upper';
    }

    return $classes;
}

add_filter( 'nav_menu_css_class', 'footer_menu_item_class', 10, 3 ); 