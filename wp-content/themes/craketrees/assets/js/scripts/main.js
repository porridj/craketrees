jQuery(document).ready(function($) {

	// var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
 //        // aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
 //        callback: function() { $('#dont-leave').foundation('open'); }

 //    });

	var $document = $(document),
    $element = $('.menu-holder'),
    className = 'fixed';

	$document.scroll(function() {
	  if ($document.scrollTop() >= 220) {
	    // user scrolled 50 pixels or more;
	    // do stuff
	    $element.addClass(className);
	  } else {
	    $element.removeClass(className);
	  }
	});
	
	$('.slick-slider').slick({
		  // centerMode: true,
		  centerPadding: '60px',
		autoplay: true,
  		autoplaySpeed: 3000,
		dots: false,
		fade: true,
		speed: 900,
		prevArrow: $('.prev'),
		nextArrow: $('.next')
	});

	// stick nav bar on scroll
	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();

		if (scroll >= 45) {
			$( '.navigation' ).addClass( 'stickit' );
		} else {
			$( '.navigation' ).removeClass( 'stickit' );
		}
		if( scroll == 0 ) {
			$('body').addClass('at_top');
		} else {
			$('body').removeClass('at_top');
		}
	});

	// scroll to page top
	$( "a[href='#top']" ).on( 'click', function(e) {
		$( "html, body") .animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$( '.woocommerce-product-gallery__image a' ).lightGallery();
	
	$( '.gallery-container' ).lightGallery({
	    thumbnail:true,
	    animateThumb: false,
	    showThumbByDefault: false
	}); 
	// $('.basket').on('click', function(event) {
	// 	event.preventDefault();
	// 	/* Act on the event */
	// 	// alert('luke sucks');
	// });
	// $( '.product-inner .hover-info' ).on( 'click' , function(event) {
	// 	// event.preventDefault();
	// 	/* Act on the event */
	// 	// alert('DAN');
	// 	// $( this ).find('a').trigger( "click" );
	// 	window.location.href=$(this).find('a').attr('href');
	// });

	// $( '.launch-lightbox' ).on('click', function(event) {
	// 	event.preventDefault();

	// 	$('.swipebox').eq(1).trigger('click');

	// });
	
	// Changing the defaults
	window.sr = ScrollReveal({ 
		// 'bottom', 'left', 'top', 'right'
		origin: 'left',

		// Can be any valid CSS distance, e.g. '5rem', '10%', '20vw', etc.
		distance: '60px',

		// Time in milliseconds.
		duration: 800,
		delay: 0,

		// Starting angles in degrees, will transition from these values to 0 in all axes.
		rotate: { x: 0, y: 0, z: 0 },

		// Starting opacity value, before transitioning to the computed opacity.
		opacity: 0,

		// Starting scale value, will transition from this value to 1
		scale: 0.8,

		// Accepts any valid CSS easing, e.g. 'ease', 'ease-in-out', 'linear', etc.
		easing: 'ease',

		// `<html>` is the default reveal container. You can pass either:
		// DOM Node, e.g. document.querySelector('.fooContainer')
		// Selector, e.g. '.fooContainer'
		container: window.document.documentElement,

		// true/false to control reveal animations on mobile.
		mobile: true,

		// true:  reveals occur every time elements become visible
		// false: reveals occur once as elements become visible
		reset: false,

		// 'always' — delay for all reveal animations
		// 'once'   — delay only the first time reveals occur
		// 'onload' - delay only for animations triggered by first load
		useDelay: 'always',

		// Change when an element is considered in the viewport. The default value
		// of 0.20 means 20% of an element must be visible for its reveal to occur.
		viewFactor: 0.6,

		// Pixel values that alter the container boundaries.
		// e.g. Set `{ top: 48 }`, if you have a 48px tall fixed toolbar.
		// --
		// Visual Aid: https://scrollrevealjs.org/assets/viewoffset.png
		viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },

		// Callbacks that fire for each triggered element reveal, and reset.
		beforeReveal: function (domEl) {},
		beforeReset: function (domEl) {},

		// Callbacks that fire for each completed element reveal, and reset.
		afterReveal: function (domEl) {},
		afterReset: function (domEl) {}
	 });

	// Customizing a reveal set
		
	sr.reveal('.sR-bottom', { origin: 'bottom' });
	sr.reveal('.sR-left', { origin: 'left' });
	sr.reveal('.sR-right', { origin: 'right' });

	sr.reveal('.sR-one', { origin: 'left', duration: 500, delay: 0, rotate: { x: 0, y: 0, z: 0 }, distance: "100px" });
	sr.reveal('.sR-two', { origin: 'bottom', duration: 500, delay: 100, rotate: { x: 0, y: 0, z: 0 }, distance: 0 });
	sr.reveal('.sR-three', { origin: 'right', duration: 500, delay: 400, rotate: { x: 0, y: 0, z: 0 }, distance: "100px" });


	

	
});