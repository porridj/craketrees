<?php get_header(); ?>
<?php get_template_part( 'parts/loop', 'banner' ); ?>
			
<div id="content" class=" row column">
	<div class="row pm">
	<header class="columns large-10 large-centered text-center">
		<h1 class="page-title">News</h1>
		<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
	</header>
	</div>	

<?php //if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumbs" class="text-center mbs">','</div>'); } ?>
	<div id="inner-content" class="row pm">
		    <main id="main" class="large-8 columns" role="main">
			<div class="bg-mediumgrey pam">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'single' ); ?>
				    
				<?php endwhile; ?>	

						
				<?php endif; ?>
			</div>
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	<?php get_template_part( 'parts/loop', 'flexible_content' ); ?>


<section class=" ">
	<div class="row mbm">
		<div class="columns text-center">
			<hr>
		
			
			<h2>More posts like this</h2>

		
		</div>
	</div>
	<div class="row medium-up-2" data-equalizer>
		<?php

			$terms = wp_get_post_terms($post->ID, 'post_tag', array("fields" => "ids"));


			$args = array( 
				'post_type' => 'post', 
				'posts_per_page' => 2,
				'post__not_in' => array($post->ID)
				 );
			
			if(!empty($terms)) {
				$args_terms = array( 
				'post_type' => 'post', 
				'posts_per_page' => 2,
				'post__not_in' => array($post->ID),
				'tag__in' => array_values($terms),
				 ); 

			}


			// $args = array( 'post_type' => 'routes', 'posts_per_page' => 4 );
			
			if(isset($args_terms)) {
				$loop = new WP_Query( $args_terms );
				if(!$loop->found_posts) {
					$loop = new WP_Query( $args );
				}
			} else {
				$loop = new WP_Query( $args );
			}

				while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>
					
		<?php endwhile;
		wp_reset_query(); ?>
	</div>

</section>

</div> <!-- end #content -->




<?php get_footer(); ?>