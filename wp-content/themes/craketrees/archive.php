<?php get_header(); ?>
	
			
	<div id="content" class=" row column">

		<?php //if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumbs" class="text-center">','</div>'); } ?>

		<div class="row pm">
		<header class="columns large-10 large-centered text-center">
    		<h1 class="page-title"><?php the_archive_title();?></h1>
			<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
    	</header>
		</div>	
	
		<div id="inner-content" class="row pbm">
		    <main id="main" class="large-8 columns" role="main">
		    	<div class="row medium-up-2" data-equalizer>
		    		
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>
				<?php endwhile; ?>	
		    	</div>

					<?php joints_page_navi(); ?>
					
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->


	</div> <!-- end #content -->

<?php get_footer(); ?>

