<?php get_header(); ?>
			
<div id="content" class="bg-white border-bottom pl">
DAN
	<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumbs" class="text-center">','</div>'); } ?>

	<div id="inner-content" class="row">

		<main id="main" class="large-8 medium-8 medium-centered columns" role="main">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

		<?php // get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->
<?php get_template_part( 'parts/loop', 'flexible_content' ); ?>

<?php get_footer(); ?>