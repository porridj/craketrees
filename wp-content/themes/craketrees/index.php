<?php get_header(); ?>
			
	<div id="content" class="row column">
		

			
		<div class="row pm">
		<header class="columns large-10 large-centered text-center">
    		<h1 class="page-title">News</h1>
			<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>
    	</header>
		</div>	

		<div id="inner-content" class="row pbm">
		    <main id="main" class="large-8 columns" role="main">
		    
		    	<div class="row medium-up-2" data-equalizer>
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->
		    
		    <?php get_sidebar(); ?>

		</div> <!-- end #inner-content -->


	</div> <!-- end #content -->

<?php get_footer(); ?>