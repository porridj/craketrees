<!doctype html>
<!-- v2 -->
  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<?php get_template_part( 'parts/head', 'icons' ); ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/scrollReveal.js'></script>
		<script type='text/javascript'>
      		window.sr = ScrollReveal();
			// Add class to <html> if ScrollReveal is supported
	      	if (sr.isSupported()) { document.documentElement.classList.add('sr'); }
	    </script>

	    <style>/* Ensure elements load hidden before ScrollReveal runs */.sr .sReveal { visibility: hidden; }</style>

	    
		<?php wp_head(); ?>


		<!-- Hotjar Tracking Code for http://www.craketreesmanor.co.uk -->
		<script>
		    (function(h,o,t,j,a,r){
		        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		        h._hjSettings={hjid:454606,hjsv:5};
		        a=o.getElementsByTagName('head')[0];
		        r=o.createElement('script');r.async=1;
		        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		        a.appendChild(r);
		    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
		</script>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96228476-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-96228476-1');
		</script>


	</head>
		
	<body <?php body_class('at_top'); ?>>

		
		<header class="header -border-bottom" role="banner">
						
			 <?php get_template_part( 'parts/nav', 'title-bar' ); ?>

		</header> <!-- end .header -->
