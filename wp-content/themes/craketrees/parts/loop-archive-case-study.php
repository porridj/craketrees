<article id="post-<?php the_ID(); ?>" <?php post_class('row mm'); ?> role="article">					
	<div class="columns medium-4">
		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
		<?php if (has_post_thumbnail()) : ?>
			<?php the_post_thumbnail('square', array('class'=>'rounded border')); ?>
		<?php else: ?>
			<?php echo wp_get_attachment_image( 218, 'square', false, array('class'=>'rounded border') ); ?>
		<?php endif; ?>
		</a>
	</div>
	<div class=" columns medium-8">
		<div class="row">
			<header class="article-header column">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			</header> <!-- end article header -->
							
			<section class="entry-content columns medium-8" itemprop="articleBody">
				<?php the_excerpt(); ?>
			</section> <!-- end article section -->
			<div class="columns medium-4">
			<?php
		       if( have_rows('key_points') ):
		       		echo '<ul>';
				    while ( have_rows('key_points') ) : the_row();
						echo '<li><strong>';
				        the_sub_field('key_point');
						echo '</strong></li>';
				    endwhile;
		       		echo '</ul>';
				endif;
			?>
			</div>
			<div class="column small-12">
				<a class="button primary " href="<?php the_permalink() ?>">View the <?php the_title(); ?> case study</a>
			</div>
		</div>

	</div>
									    						
</article> <!-- end article -->