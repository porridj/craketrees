<?php $grid_links_row++; ?>
<?php $grid_links = get_sub_field('grid_links'); ?>
<?php $i = 0; ?>

<?php if( $grid_links ): ?>
	<?php if(count($grid_links) == 4) {
		$large_up = 2;
	} else {
		$large_up = 3;
	} ?>

	<div class="row expanded collapse small-up-1 medium-up-2 large-up-<?php echo $large_up ?> border-bottom-" id="grid-links-<?php echo $grid_links_row; ?>">

			<?php foreach( $grid_links as $grid_link): $i++; ?>
				
				<div class="column grid-link">
					<a href="<?php echo $grid_link['grid_link']; ?>">
						<?php
							 echo wp_get_attachment_image( $grid_link['grid_link_image'], 'medium-crop' );
						?>
						<span><?php echo $grid_link['grid_link_title']; ?></span>
					</a>
				</div><!-- /.column -->

			<?php endforeach; ?>


	</div>

<?php endif; ?>



				