<article id="post-<?php the_ID(); ?>" <?php post_class('mm'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						

	<header class="article-header ps">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
<?php //if ( function_exists('yoast_breadcrumb') && !is_front_page() && !is_page('stay-with-us') ) { yoast_breadcrumb('<div id="breadcrumbs" class="text-center">','</div>'); } ?>
	</header> <!-- end article header -->
    <section class="entry-content text-center" itemprop="articleBody">
	    <?php the_content(); ?>
	    <?php wp_link_pages(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->