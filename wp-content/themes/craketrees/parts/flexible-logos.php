
<?php 
$logos++;
$logos_array = get_sub_field('logos');

if( $logos_array ): $i = 0; ?>
<div class=" pl border-bottom" id="gallery-<?php echo $logos; ?>">
	
	<div class="row text-center brand-logo-gallery">

		<div class="column small-12">
			<h2 class="upper"><?php the_sub_field('logo_title'); ?></h2>
		</div>

		<div class="column small-12">

			<?php foreach( $logos_array as $logo ): $i++; ?>
				<div class="va-mid inline">
						
					<?php if ( $logo['logo_link'] ) : ?><a href="<?php echo $logo['logo_image']['logo_link']; ?>" title="<?php echo $logo['alt']; ?>" target="_blank"><?php endif; //open link if there is one ?>
						<img src="<?php echo $logo['logo_image']['sizes']['logo']; ?>" width="<?php echo $logo['logo_image']['sizes']['logo-width']/2; ?>"  height="<?php echo $logo['logo_image']['sizes']['logo-height']/2; ?>" alt="<?php echo $logo['alt']; ?>" class="logo" />
					<?php if ( $logo['logo_link'] ) : ?></a><?php endif; //close link if there was one ?>

				</div>
			<?php endforeach; ?>

		</div>
	</div>

</div>
<?php endif; ?>