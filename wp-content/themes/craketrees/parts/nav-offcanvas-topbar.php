<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar " id="top-bar-menu">
<div class="row ">
<div class="columns">
	<div class="top-bar-left float-left">
		<a href="<?php echo home_url(); ?>">
			<?php 
			$logo = get_field('logo', 'option');
			$width = get_field('logo_width', 'option');
			if( empty($width) ) $width = '300';

			if( !empty($logo) ) : ?>

				<img src="<?php echo $logo; ?>" title="<?php bloginfo('name'); ?>" width="<?php echo $width ?>">

			<?php else: ?>

				<h1><?php bloginfo('name'); ?></h1>
				
			<?php endif; ?>
		</a>
	</div>
	<div class="top-bar-right show-for-medium">
		<?php joints_top_nav(); ?>	
	</div>
	<div class="top-bar-right float-right show-for-small-only">
		<ul class="menu">
			<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
</div><!-- /.columns -->
</div><!-- /.row -->
</div>