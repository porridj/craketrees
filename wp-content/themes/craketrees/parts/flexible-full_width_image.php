<?php $full_width_image++; ?>

<style type="text/css">

	<?php
		$slide_post 	= get_sub_field('full_width_image');
		$url 			= wp_get_attachment_image_src( $slide_post, 'banner-large');
		$url_med		= wp_get_attachment_image_src( $slide_post, 'banner-medium');
		$url_small 		= wp_get_attachment_image_src( $slide_post, 'banner-small');
		$link 			= get_sub_field('fwi_link');
		$fwi_button_text= get_sub_field('fwi_button_text');
		$fwi_text 		= get_sub_field('fwi_text');
	?>

	#full-width-image-<?php echo $full_width_image; ?> { 
		background-image: url(<?php echo $url_small[0]; ?>); }
	@media(min-width: 720px) { 
	#full-width-image-<?php echo $full_width_image; ?> { 
		background-image: url(<?php echo $url_med[0]; ?>); } 
	}
	@media(min-width: 1024px) { 
	#full-width-image-<?php echo $full_width_image; ?> { 
		background-image: url(<?php echo $url[0]; ?>); } 
	}

</style>
<div class="row " >
	<div class="column">
		
<div class=" full-width-image mh400 flex-center " id="full-width-image-<?php echo $full_width_image; ?>">

	<div class="pl">
		<?php if ($fwi_text): ?>
			
		<div class="row advert">

			<div class="small-12 medium-12 large-12 columns text-center">

				<h2><?php echo $fwi_text; ?></h2>
			
				<?php $url = get_permalink( $link ); ?>
				<?php if($fwi_button_text) : ?><a href="<?php echo $url; ?>" class="button white "><?php echo $fwi_button_text; ?></a><?php endif; ?>
			</div>
		</div>

		<?php endif ?>
	</div>

	</div>
</div>
</div>

