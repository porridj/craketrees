<?php $latest_posts++; ?>
<?php $category_ids = get_sub_field('latest_posts'); ?>
<?php $i = 0; ?>
<?php $id = get_the_ID(); ?>
<?php if(!$id) $id = 0; ?>

<?php
	$posts_per_page = 1;

	$args = array(
	    'category__in'     	=> $category_ids,
		'post_type'   		=> 'post',
		'post_status' 		=> 'publish',
		'order'             => 'DESC',
		'orderby'           => 'date',
		'post__not_in'		=> array($id),
		'posts_per_page'    => 3,

	);

	$post_query = new WP_Query( $args ); 
?>

<?php if ( $post_query->have_posts() ) : ?>
	<div class="mm" id="latest_post-<?php echo $latest_posts ?>">
	<?php while ( $post_query->have_posts() ): $post_query->the_post(); ?>

	 
		<?php 
		// Adjust the amount of rows in the grid
		$grid_columns = 3; ?>

		<?php if( 0 === ( $post_query->current_post  )  % $grid_columns ): ?>

		    <div class="row medium-up-3 archive-grid" data-equalizer>

		<?php endif; ?> 

			<!-- To see additional archive styles, visit the /parts directory -->
			<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>

								
		<?php if( 0 === ( $post_query->current_post + 1 )  % $grid_columns ||  ( $post_query->current_post + 1 ) ===  $post_query->post_count ): ?>

		     <!--End Row: --> </div>

		<?php endif; ?>
		    


	<?php endwhile; ?>
    </div>
<?php endif; ?>

<?php wp_reset_query(); ?>







				