<?php 
	$accommodation++;
	$image = get_sub_field('acc_main_image');
	$text = get_sub_field('acc_text');
	$link = get_sub_field('acc_link');
	$gallery = get_sub_field('acc_gallery');
	$title = get_sub_field('acc_title');
	$features = get_sub_field('acc_features');

?>



<?php if( $image && $text ): ?>

	<div class="accomodation-row" id="accommodation-<?php echo $accommodation; ?>">

				<?php
					$thumb = wp_get_attachment_image_src( $image, 'large' );

					if($thumb) $url = 'background-image: url('.$thumb['0'].');';
					
					if($accommodation%2 == 0) { 
						// $class_image = 'medium-push-8';
						$class_image = 'large-push-5';
						$class_content = 'large-pull-7';
					} else {
						$class_content = '';
						$class_image = '';
					}
				?>
					
				<div class="row mbm relative large-flex-stretch ">

					<div class="columns small-12 large-7 relative <?php echo $class_image; ?>">
						<div class="relative image-link " style="height: 100%;">
							
						<a href="<?php echo $link; ?>" style="<?php echo $url; ?>" class="">
						</a>

						</div>

					</div>

					<div class=" columns small-12 large-5 <?php echo $class_content; ?> ">
						<div class="pam bg-mediumgrey flex-center text-center">
							<div>
								
							<?php 
							$feature_list = array();
							if( $features ) :

								foreach($features as $feature) {
									$feature_list[] = '<i class="fa fa-2x fa-'.$feature.'" aria-hidden="true"></i> ';
								} ?>
								<?php //echo implode('', $feature_list) ?>
							<?php endif; ?>
							<h2><?php echo $title ?></h2>
							<div class="">
								<?php echo $text ?>
								
							</div>
							<?php if($gallery) : ?>
								<div class="row small-up-5 ml collapse gallery-container">
									
								<?php foreach ($gallery as $key => $gallery_image): ?>

									<a href="<?php echo $gallery_image['sizes']['swipebox'];  ?>" class=" column " rel="gallery-<?php echo $accommodation ?>">
										<img src="<?php echo $gallery_image['sizes']['post-thumbnail'];  ?>" class="border">
									</a>
								<?php endforeach ?>
								</div>
							<?php endif; ?>
							<?php if($link): ?><p class=""><a href="<?php echo $link ?>" class="button  secondary expanded large" target="_blank">Book online</a><?php endif; ?></p>
							</div>
						</div>
					</div>
					
				</div><!-- row -->



	</div>

<?php endif; ?>



				