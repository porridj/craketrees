<?php 
	// check if the flexible content field has rows of data
	if( have_rows('flexible_content') ): 

		$latest_posts = 0;
		$large_featured_links = 0;
		$testimonials_row = 0;
		$gallery_row = 0;
		$full_width_image = 0;
		$faq = 0;
		$text_block_row = 0;
		$downloads = 0;
		$featured_products = 0;
		$grid_links_row = 0;
		$feature_row = 0;
		$logo_section = 0;
		$newsletter_block = 0;
		$map = 0;
		$accommodation = 0;
		$instagram = 0;
		$spacer = 0;
		$features_row = 0;

	    // loop through the rows of data
	    while ( have_rows('flexible_content') ) : the_row();

	        if( get_row_layout() == 'latest_posts' ):

		       	include( locate_template( 'parts/flexible-latest_posts.php', false, false) ); 

		    elseif( get_row_layout() == 'map' ):

		       	include( locate_template( 'parts/flexible-map.php', false, false) ); 

		    elseif( get_row_layout() == 'spacer' ):

		       	include( locate_template( 'parts/flexible-spacer.php', false, false) ); 

			elseif( get_row_layout() == 'instagram' ):

		       	include( locate_template( 'parts/flexible-instagram.php', false, false) ); 

	       	elseif( get_row_layout() == 'large_featured_links' ):

	        	include( locate_template( 'parts/flexible-large_featured_links.php', false, false) ); 

	        elseif( get_row_layout() == 'gallery' ):

	        	include( locate_template( 'parts/flexible-gallery.php', false, false) ); 

			elseif( get_row_layout() == 'full_width_image' ):
	
	        	include( locate_template( 'parts/flexible-full_width_image.php', false, false) ); 
			
			elseif( get_row_layout() == 'faqs' ):

				include( locate_template( 'parts/flexible-faq.php', false, false) ); 
			
			elseif( get_row_layout() == 'text_block' ):
	      	
	        	include( locate_template( 'parts/flexible-text_block.php', false, false) ); 

			elseif( get_row_layout() == 'downloads' ):
	      	
	        	include( locate_template( 'parts/flexible-downloads.php', false, false) );

	        elseif( get_row_layout() == 'grid_links' ):
	      	
	        	include( locate_template( 'parts/flexible-grid_links.php', false, false) ); 

	        elseif( get_row_layout() == 'feature_row' ):
	      	
	        	include( locate_template( 'parts/flexible-feature_row.php', false, false) ); 

	        elseif( get_row_layout() == 'testimonials' ):
	      	
	        	include( locate_template( 'parts/flexible-testimonials.php', false, false) ); 

	        elseif( get_row_layout() == 'logo_section' ):
	      	
	        	include( locate_template( 'parts/flexible-logos.php', false, false) );

	        elseif( get_row_layout() == 'newsletter_block' ):
	      	
	        	include( locate_template( 'parts/flexible-newsletter_block.php', false, false) ); 

	        elseif( get_row_layout() == 'accommodation' ):

	        	include( locate_template( 'parts/flexible-accommodation.php', false, false) ); 

	       	elseif( get_row_layout() == 'features' ):

	        	include( locate_template( 'parts/flexible-features.php', false, false) ); 
	
	        endif;

	    endwhile;

	endif; ?>

