<?php $spacer++; ?>
<?php 
	$size 			= get_sub_field('size');
	$border 		= get_sub_field('border');
?>

<hr class="spacer <?php echo $size; ?> <?php echo $border; ?>">
