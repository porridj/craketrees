
<?php 
	$features_row++;
	$features = get_sub_field('features');
?>


<?php if( !empty($features) ) : ?>
<div class="mm" id="features_row-<?php echo $features_row; ?>">

	<div class="row  <?php echo $block_grid; ?> small-up-1 medium-up-3  ">

		<?php foreach( $features as $feature):  ?>
			<div class="column text-center mbm">
				<div class="maxw">
					<h3 class="mtl mbm">
						<?php echo $feature['title']; ?>	
					</h3>

					<?php if(!empty($feature['content'])): ?>
					<div class="content mbs">
						<?php echo $feature['content']; ?>
					</div>
					<?php endif; ?>

					<?php if(!empty($feature['image']['sizes']['banner-small'])): ?>
					<div class="image mbs">
						<img src="<?php echo $feature['image']['sizes']['banner-small']; ?>">
					</div>
					<?php endif; ?>
					
					<?php if(!empty($feature['link']['url'])): ?>
						<a href="<?php echo $feature['link']['url']; ?>" target="<?php echo $feature['link']['target']; ?>" class="underline ">

						<?php if(!empty($feature['link']['title'])): ?>
							<?php echo $feature['link']['title']; ?>
						<?php else: ?>
							Find out more
						<?php endif; ?>
						</a>
					<?php endif; ?>

				</div>
			</div>
		<?php endforeach; ?>
	</div>

</div>
<?php endif ; ?>
