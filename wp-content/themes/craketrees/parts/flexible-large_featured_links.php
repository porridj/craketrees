<?php $large_featured_links++; ?>
<?php $featured_links = get_sub_field('large_featured_links'); ?>
<?php $i = 0; ?>

<?php if( $featured_links ): ?>

	<div class="" id="large_featured_links-<?php echo $large_featured_links; ?>">

			<?php foreach( $featured_links as $post_object): $i++; ?>


				<?php
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post_object['large_featured_link']->ID), 'large' );
					if($thumb) $url = 'background-image: url('.$thumb['0'].');';
					
					if($i%2 == 0) { 
						// $class_image = 'medium-push-8';
						$class_image = 'large-push-7 border-left';
						$class_content = 'large-pull-5';
						$bg_colour = 'bg-primary';
					} else {
						$class_content = '';
						$class_image = 'border-right';
						$bg_colour = 'bg-darkgray';
					}
				?>
					
				<div class="row collapse expanded relative border-bottom <?php echo $bg_colour; ?>">

					<div class="columns small-12 large-5 image-link  <?php echo $class_image; ?>">
						<a href="<?php echo get_permalink($post_object['large_featured_link']->ID); ?>" style="<?php echo $url; ?>"></a>
					</div>

					<div class=" columns small-12 large-7 h400 hauto flex-center  <?php echo $class_content; ?>">
						<div class="pm column maxw small-12">
							<h2 class="big"><a href="<?php echo get_permalink($post_object['large_featured_link']->ID); ?>"><?php echo get_the_title($post_object['large_featured_link']->ID); ?></a></h2>
							<p class=" text-justify"><?php the_field('page_summary', $post_object['large_featured_link']->ID); ?></p>
							<a href="<?php echo get_permalink($post_object['large_featured_link']->ID); ?>" class="button white secondary">Find out more</a>
						</div>
					</div>
					
				</div><!-- row -->

			<?php endforeach; ?>


	</div>

<?php endif; ?>



				