
<?php 
$gallery_row++;
$title = get_sub_field('title');
$description = get_sub_field('description');
$images = get_sub_field('gallery');


if( $images ): ?>
	<div class="bg-mediumgrey ptm">
		<?php if($title || $description ): ?>
		<div class="row mbm">
			<div class="column medium-centered medium-8 text-center">
				<?php if($title): ?><h2><?php echo $title; ?></h2><?php endif; ?>
				<?php if($description): ?><p><?php echo $description; ?></p><?php endif; ?>
			</div>
		</div>
		<?php endif; ?>
		<div class=" row text-center gallery small-up-3 gallery-container" id="gallery-<?php echo $gallery_row; ?>">

			<?php foreach( $images as $image ): ?>
				<div class="column mbm" data-src="<?php echo $image['sizes']['swipebox']; ?>">
					<a 
					href="<?php echo $image['sizes']['swipebox']; ?>" 
					class="swipebox gallery-image- " 
					title="<?php echo $image['caption']; ?>" 
					rel="gallery-strip-<?php echo $gallery_row ?>">
						<img src="<?php echo $image['sizes']['square']; ?>" alt="<?php echo $image['alt']; ?>" class="" />
					</a>
				</div>

			<?php endforeach; ?>

		</div>
	</div>


<?php endif; ?>