<?php 
$testimonials_row++;
$i = 0; 
?>
<?php 
	$testimonials = get_sub_field('testimonials'); 
	$count = count($testimonials);
?>
<!-- testimonial -->
<?php if($testimonials): ?>
	<div class="bg-mediumgrey pam" id="testimonial-<?php echo $testimonials_row; ?>">
	<?php if($count === 1): ?>

		<!-- testimonial -->
		<div class="row">
			
			<div class="columns small-12 medium-8 medium-centered text-center testimonial">


				<div class="speech left"><i class="fa fa-quote-left"></i></div>
				<div class="speech right"><i class="fa fa-quote-right"></i></div>
				<h3><?php echo $testimonials[0]['testimonial_text'] ?></h3>
				<p class=" text-center"><?php echo wp_get_attachment_image( $testimonials[0]['testimonial_image'], 'tiny', false, array('class' => 'circle') ); ?> - <em><?php echo $testimonials[0]['testimonial_name'] ?></em></p>

			</div>
		</div><!-- /.row -->

	<?php else: ?>
		<div class="row mbm">
			<div class="column medium-centered medium-8 text-center">
				<h2>
					From our guests...
				</h2>
			</div>
		</div>
		<div class="row small-up-1 medium-up-2 large-up-<?php echo $count; ?>">
				

			<?php foreach ($testimonials as $key => $testimonial_single) : ?>
				
				<div class="columns text-center">
				<div class="mini-testimonial">

					<div class="stars mbs">
						<i class="fa fa-star" aria-hidden="true"></i> 
						<i class="fa fa-star" aria-hidden="true"></i> 
						<i class="fa fa-star" aria-hidden="true"></i> 
						<i class="fa fa-star" aria-hidden="true"></i> 
						<i class="fa fa-star" aria-hidden="true"></i> 
					</div>
					<div class="text text-center">
						<?php echo $testimonial_single['testimonial_text'] ?>
					</div>
					<hr>
					<p>
						<?php echo wp_get_attachment_image( $testimonial_single['testimonial_image'], 'tiny' ); ?> - <em><?php echo $testimonial_single['testimonial_name'] ?></em>
					</p>
				</div>
				</div>

			<?php endforeach ?>
				
		</div>
	<?php endif; ?>
</div><!-- /row / testimonial -->
<?php endif; ?>






