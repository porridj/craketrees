<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-position="right">
	<a href="<?php echo home_url(); ?>">
		<?php 
		$logo = get_field('logo', 'option');

		if( !empty($logo) ) : ?>

			<img src="<?php echo $logo; ?>" title="<?php bloginfo('name'); ?>">

		<?php else: ?>

			<h1><?php bloginfo('name'); ?></h1>
			
		<?php endif; ?>
	</a>
	<?php joints_off_canvas_nav(); ?>
</div>