

		<!--Item: -->
		<div class="columns mbm">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class('text-center bg-mediumgrey pam '); ?> role="article"  data-equalizer-watch>
			
				<section class=" mbs" itemprop="articleBody">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<?php if (has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('square'); ?>
					<?php else: ?>
						<?php echo wp_get_attachment_image( 170, 'square' ); ?>
					<?php endif; ?>
					</a>
				</section> <!-- end article section -->
				<header class="article-header">
					<h4 class="title ">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="light-text">
					<?php the_title(); ?>
					</a>
					</h4>	
				</header> <!-- end article header -->	
				<main class="pbm">
					<?php the_excerpt(); ?>
					<p><a href="<?php the_permalink() ?>">
								<i class="fal fa-2x fa-chevron-circle-right"></i>
					</a></p>
				</main>
						
			</article> <!-- end article -->
			
		</div>
