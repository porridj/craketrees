
<?php 
	$text_block_row++;
	$text_blocks = array();
	$text_block = get_sub_field('text_block'); 
	$text_block_2 = get_sub_field('text_block_2'); 
	$text_block_3 = get_sub_field('text_block_3'); 
	$text_block_4 = get_sub_field('text_block_4'); 
	$text_blocks = array(
		$text_block,
		$text_block_2,
		$text_block_3,
		$text_block_4
	);
	$text_blocks= array_filter($text_blocks);	
	$background_colour = get_sub_field('background'); 
	$columns = get_sub_field('columns');
	if( $columns == 2 )  { $block_grid = 'large-up-2'; }
	if( $columns == 3 )  { $block_grid = 'large-up-3'; }
	if( $columns == 4 )  { $block_grid = 'medium-up-2 large-up-4'; }
?>


<div class="<?php echo $background_colour;?> <?php if($background_colour == 'default') { echo 'mm'; } else { echo 'pm'; } ?> " id="text_block_row-<?php echo $text_block_row; ?>">

	<?php if ($columns === "1"): ?>
			
		<div class="row ps">
			<div class="column small-12 medium-10 large-8 medium-centered  text-center">
				<div class="pam">
					<?php echo $text_block; ?>
				</div>
			</div><!-- /.column -->
		</div>

	<?php else: ?>

		<?php if( !empty($text_blocks) ) : ?>

			<div class="row  <?php echo $block_grid; ?> small-up-1  ">
				<?php $count = 0; ?>
				<?php foreach( $text_blocks as $text): $count++; if($count > $columns) break; ?>
					<div class="column text-center">
						<div class="maxw">
							<?php echo $text; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

		<?php endif ; ?>

	<?php endif ?>

</div>
