
<?php if( have_rows('banner_slide') ): ?>
	<?php 
		$slide_text = the_sub_field('slide_text');
		$slide_link = the_sub_field('slide_link');
	?>

	<div class="banner ">

	<?php 
	 $slides = get_field('banner_slide');
	?>

		<div class="slick-wrap ">
			<?php if(count($slides)>1): ?>
			<div class="prev"><i class="fa fa-angle-left"></i></div>
			<div class="next"><i class="fa fa-angle-right"></i></div>
			<?php endif; ?>

			<div class="slick-slider">

				<?php $slide = 0; ?>

				<?php while( have_rows('banner_slide') ): the_row(); $slide++; ?>
					<?php
					$slide_post 	= get_sub_field('slide_image');
					$url 			= wp_get_attachment_image( $slide_post, 'banner-large');
					$slide_text		= get_sub_field('slide_text');
					$slide_link		= get_sub_field('slide_link');
					?>
					<div class="slide">

						<div class="slide-image slide-<?php echo $slide; ?>">
							<?php echo $url; ?>
						</div>

						<?php if( !empty( $slide_text  ) ) : ?>
								<div class="slide-details text-center">
									<h2>
										<?php echo $slide_text;  ?>
									</h2>
								</div>
						<?php endif; ?>

					</div>

				<?php endwhile; ?>

				
			</div>
		</div><!-- slick_wrap -->
	</div><!-- banner -->
<?php endif; ?>