
<?php $downloads++; ?>
<div class="border-bottom">
	
<div class="row pm" id="downloads-<?php echo $downloads; ?>">
				
	<div class="columns small-12 medium-8 medium-offset-2">
	 	<h2 class=" text-center">Downloads</h2>
		<hr>
		<ol class="inside">
		    <?php while( have_rows('downloads') ): the_row(); ?>
		 	
				<?php         
		        $download 	= get_sub_field('download'); 
		        ?>
		        
		        <li>
					<?php if( !empty($download) ) : ?>
						<p>
							<a href="<?php echo $download['url']; ?>">Download: <?php echo $download['title']; ?></a> ( <small><?php echo $download['filename']; ?></small> )
							<?php if(!empty($download['description'])) : ?>
								<br>
								<small>
									<?php echo $download['description']; ?>
								</small>
							<?php endif; ?>
						</p>
					<?php endif; ?>
		        </li>

		    <?php endwhile; ?>
		</ol>
	</div>

</div> 
</div>