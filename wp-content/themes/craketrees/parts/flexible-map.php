<?php $map++; ?>
<?php 
	$show_map 			= get_sub_field('show_map');
	$custom_locations 	= get_sub_field('custom_location');
	$location 			= get_field('location', 'option');
?>

<div class="border-bottom-" id="map_<?php echo $map;?>">
	<div class="map">
		<div class="acf-map">
		
			<?php if( $show_map == 'custom' ):	?>
				<?php foreach ($custom_locations as $key => $custom_location) : ?>

					<div class="marker" data-lat="<?php echo $custom_location['location_marker']['lat']; ?>" data-lng="<?php echo $custom_location['location_marker']['lng']; ?>"></div>	
				<?php endforeach; ?>
			<?php else: ?>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
			<?php endif; ?>

		</div>
	</div>
</div>