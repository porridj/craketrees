<article id="post-<?php the_ID(); ?>" <?php post_class('column mbm'); ?> role="article">	
<div class="bg-mediumgrey pam" data-equalizer-watch>
	
	<header class="article-header">
		<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('banner-small'); ?></a>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

	</header> <!-- end article header -->
					
	<section class="entry-content " itemprop="articleBody">
		<?php the_excerpt(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->		
				    <hr class="ml">

</div>				
</article> <!-- end article -->