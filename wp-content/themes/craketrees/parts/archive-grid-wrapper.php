<?php 
// Adjust the amount of rows in the grid
$grid_columns = 3; ?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

    <div class="row archive-grid" data-equalizer>

<?php endif; ?> 

	<!-- To see additional archive styles, visit the /parts directory -->
	<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>

						
<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

     <!--End Row: --> </div>

<?php endif; ?>


