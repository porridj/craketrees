
<?php $faq++; ?>
<div class="bg-darkgrey-">
	
<div class="row mm" id="faq-<?php echo $faq; ?>">
				
	<div class="columns small-12 medium-8 medium-offset-2">
	 	<h2 class="mbm text-center">Frequently Asked Questions</h2>

		<ol class="inside accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
		    <?php while( have_rows('faqs') ): the_row(); ?>
		 	
				<?php         
		        $faq_question 	= get_sub_field('question'); 
		        $faq_answer 	= get_sub_field('answer'); 
		        ?>
		        
		        <li class="accordion-item" data-accordion-item>
					<?php if( !empty($faq_question) ) : ?><a class="accordion-title"><?php echo $faq_question; ?></a>	<?php endif; ?>
					<?php if( !empty($faq_answer) ) : ?><div class="faq-answer text-justify accordion-content" data-tab-content><?php echo $faq_answer; ?></div><?php endif; ?>
		        </li>

		    <?php endwhile; ?>
		</ol>
	</div>

</div> 
</div>
