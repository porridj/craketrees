<?php 
	$newsletter_block++;
	$newsletter_text = get_sub_field('newsletter_text'); 
?>
<?php if( !empty($newsletter_text) ) : ?>
	<!-- newsletter -->
	<div class=" pl bg-primary border-bottom" id="newsletter_block-<?php echo $newsletter_block; ?>">
		<div class="row">
			<div class="columns small-12 newsletter">
				<?php if( !empty($newsletter_text) ) : ?>
				<div class="row">
					<div class="columns small-12 medium-6 medium-centered text-center">
					<?php echo $newsletter_text; ?>
					</div><!-- /.columns -->
				</div><!-- /.row -->
				<?php endif; ?>

				<div id="mc_embed_signup">
					<form action="//dogtooth.us2.list-manage.com/subscribe/post?u=5342547e317232fe5cd008b50&amp;id=bdceb1fce2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate subscribeForm" target="_blank" novalidate="">
					    <div id="mc_embed_signup_scroll">
					    <div class="row ">
							<div class="mc-field-group columns small-12 medium-4">
							    <!-- <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span> -->
								</label>
							    <input type="email" value="" name="EMAIL" placeholder="Email" class="required email" id="mce-EMAIL">
							</div>
							<div class="mc-field-group columns small-12 medium-4">
							    <!-- <label for="mce-FNAME">Name </label> -->
							    <input type="text" value="" name="FNAME" placeholder="Name"  class="" id="mce-FNAME">
							</div>
						    <div class="columns small-12 medium-4">
							    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button white hollow expanded light-text">
							 </div>
						    <div id="mce-responses" class="clear">
							    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_5342547e317232fe5cd008b50_bdceb1fce2" tabindex="-1" value=""></div>
						        <div class="response" id="mce-error-response" style="display:none"></div>
						        <div class="response" id="mce-success-response" style="display:none"></div>
							    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							</div><!-- /.row -->
					    </div>
					</form>
				</div>

			</div>
		</div><!-- /.row -->
	</div><!-- / newsletter -->
<?php endif; ?>

