<?php 
	$i = 0; 
	$feature_row++; 
	$image 					= get_sub_field('image'); 
	$text 					= get_sub_field('text'); 
	$type 					= get_sub_field('row_type'); 
	$link 					= get_sub_field('link'); 
	$title 					= get_sub_field('title'); 
	$columns = array(0 => '', 1 => '');
?>





<div class="row collapse mm feature-row" id="feature_row-<?php echo $feature_row; ?>">
	<div class="column small-12 medium-7 <?php if($type == 'image-right') echo 'medium-push-5'; ?>">

		<?php if($link): ?><a href="<?php echo $link['url'] ?>" target="<?php echo $link['target']; ?>"><?php endif; ?>
		<div class="image-container" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, $size = 'large' )[0] ?>);">
			
		</div>
		<?php if($link): ?></a><?php endif; ?>

	</div>
	<div class="column small-12 medium-5  <?php if($type == 'image-right') echo 'medium-pull-7'; ?> text-center">
		<div class="bg-mediumgrey pam flex-center content-container">
			<div class="">
				<h1>
					<?php if($link): ?><a href="<?php echo $link['url'] ?>"><?php endif; ?>
					<?php echo $title ?>	
					<?php if($link): ?></a><?php endif; ?>
				</h1>
				<?php echo $text; ?>
				<?php if($link): ?>
						<?php if($link['title']): ?>
							<a href="<?php echo $link['url'] ?>" target="<?php echo $link['target']; ?>" class="button mtm secondary expanded large">
								<?php echo $link['title']; ?>
							</a>
						<?php else: ?>
							<a href="<?php echo $link['url'] ?>" target="<?php echo $link['target']; ?>">
								<i class="fal fa-2x fa-chevron-circle-right"></i>
							</a>
						<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>



				