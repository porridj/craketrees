<?php
// Adjust the breakpoint of the title-bar by adjusting this variable
$breakpoint = "large"; ?>

<div data-sticky-container>
	<div data-sticky data-options="marginTop:0;" style="width:100%" data-top-anchor="0" data-sticky-on="large">

		<div class="top-bar">
			<div class="row pm align-middle-large">

				<div class="small-12 large-4 columns mtm text-left show-for-large">
					<h6 class="mz"><a href="<?php the_permalink( 10 ) ?>" class="underline">Contact</a> | Call us: <a href="tel:+441931715205">01931 715205</a></h6>
				</div>

				<div class="columns small-12 large-4 logo-container text-center relative">
					<a href="<?php echo home_url(); ?>" class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/craketreesmanor-logo-white.svg" title="<?php bloginfo('name'); ?>" width="200">
					</a>
					<h6 class="mtm mbs hide-for-large">Call us: <a href="tel:+441931715205">01931 715205</a></h6>
					<div class="" data-responsive-toggle="top-bar-menu" data-hide-for="<?php echo $breakpoint ?>">
					  <button class="menu-icon" type="button" data-toggle></button>
					</div>
				</div>

				<div class="small-12 large-4 columns mtm">
					<div class="row align-middle-large">
						
						<div class="columns large-6 ">
							<?php joints_book_nav(); ?>
						</div>


						<div class="columns small-6 text-left show-for-large">
							<?php $facebook 	= get_field('facebook', 'option'); ?>
							<?php $twitter 		= get_field('twitter', 'option'); ?>
							<?php $instagram 	= get_field('instagram', 'option'); ?>
							<?php $tripadvisor 	= get_field('tripadvisor', 'option'); ?>

							<?php if ( $facebook || $twitter || $instagram || $tripadvisor ) : ?>
								<div class="social light-links mbs inline">
									<?php if ( $facebook ) : ?><a href="<?php echo $facebook; ?>" target="_blank" class=""><i class="fab fa-facebook fa-fw"></i></a><?php endif; ?>
									<?php if ( $tripadvisor ) : ?><a href="<?php echo $tripadvisor; ?>" target="_blank" class=""><i class="fab fa-tripadvisor fa-fw"></i></a><?php endif; ?>
									<?php if ( $twitter ) : ?><a href="<?php echo $twitter; ?>" target="_blank" class=""><i class="fab fa-twitter fa-fw"></i></a><?php endif; ?>
									<?php if ( $instagram ) : ?><a href="<?php echo $instagram; ?>" target="_blank" class=""><i class="fab fa-instagram fa-fw"></i></a><?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>	
				
			</div>
		</div>

		<div class="menu-container" id="top-bar-menu">
			<div class="menu-holder bg-mediumgrey">
				<div class="row">
					<div class="columns navigation-container">
						<div class=" menu-centered">
							<?php joints_top_nav(); ?>
						</div>			
					</div>
				</div>
			</div>
			<div class="menu-holder bg-altgrey bt-grey">
				<div class="row">
					<div class="columns navigation-container" id="top-bar-submenu">
						<div class=" menu-centered">
							<?php joints_sub_nav(); ?>
						</div>			
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="sticky-call text-center hide-for-large" >
	<?php if(!is_page(12)) : ?>
	<a href="<?php the_permalink(12); ?>" class="button expanded large mz">Book online</a>
	<?php else: ?>
	<a href="tel:+441931715205" class="button expanded large mz">Call us: 01931 715205</a></a>
	<?php endif; ?>
</div>
