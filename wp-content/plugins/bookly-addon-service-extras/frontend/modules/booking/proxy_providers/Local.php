<?php
namespace BooklyServiceExtras\Frontend\Modules\Booking\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Frontend\Modules\Booking\Proxy;

/**
 * Class Local
 * @package BooklyServiceExtras\Frontend\Modules\Booking\ProxyProviders
 */
class Local extends Proxy\ServiceExtras
{
    /**
     * @inheritdoc
     */
    public static function getStepHtml( BooklyLib\UserBookingData $userData, $show_cart_btn, $info_text, $progress_tracker )
    {
        $chain = array();
        $chain_price = null;
        foreach ( $userData->chain->getItems() as $chain_item ) {
            $extras = array();
            /** @var BooklyLib\Entities\Service $service */
            $service = BooklyLib\Entities\Service::query()->where( 'id', $chain_item->getServiceId() )->findOne();
            if ( $service->getType() == BooklyLib\Entities\Service::TYPE_COMPOUND ) {
                $sub_services = $service->getSubServices();
                $service_id   = $sub_services[0]->getId();

                // Only unique extras.
                foreach ( $sub_services as $sub_service ) {
                    foreach ( (array) BooklyLib\Proxy\ServiceExtras::findByServiceId( $sub_service->getId() ) as $item ) {
                        $extras[ $item->getId() ] = $item;
                    }
                }
                $chain_price += $service->getPrice();
            } else {
                $service_id = $service->getId();
                if ( count( $chain_item->getStaffIds() ) == 1 ) {
                    $staff_service = BooklyLib\Entities\StaffService::query()
                        ->select( 'price' )
                        ->where( 'service_id', $service->getId() )
                        ->where( 'staff_id', current( $chain_item->getStaffIds() ) )
                        ->fetchRow();
                    $chain_price += $staff_service['price'];
                }
                $extras = (array) BooklyLib\Proxy\ServiceExtras::findByServiceId( $service_id );
            }
            $chain[] = array(
                'service_title'  => $service->getTranslatedTitle(),
                'service_id'     => $service_id,
                'extras'         => $extras,
                'checked_extras' => $chain_item->getExtras(),
            );
        }
        $show = get_option( 'bookly_service_extras_show' );

        return self::renderTemplate( 'step_extras', compact( 'chain', 'show', 'show_cart_btn', 'info_text', 'progress_tracker', 'chain_price' ), false );
    }
}