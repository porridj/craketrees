<?php
namespace BooklyServiceExtras\Frontend\Modules\Booking\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Frontend\Modules\Booking\Proxy;
use BooklyServiceExtras\Lib;

/**
 * Class Shared
 * @package BooklyServiceExtras\Frontend\Modules\Booking\ProxyProviders
 */
class Shared extends Proxy\Shared
{
    /**
     * @inheritdoc
     */
    public static function enqueueBookingAssets()
    {
        $link_style = get_option( 'bookly_gen_link_assets_method' ) == 'enqueue' ? 'wp_enqueue_style' : 'wp_register_style';
        $version   = Lib\Plugin::getVersion();
        $bookly_url  = plugins_url( '', Lib\Plugin::getMainFile() );
        call_user_func( $link_style, 'bookly-extras', $bookly_url . '/frontend/modules/booking/resources/css/bookly-extras.css', array( 'bookly-main' ), $version );
    }

    /**
     * @inheritdoc
     */
    public static function printBookingAssets()
    {
        wp_print_styles( 'bookly-extras' );
    }

    /**
     * @inheritdoc
     */
    public static function prepareChainItemInfoText( $data, BooklyLib\ChainItem $chain_item )
    {
        $titles = array();
        $extras = $chain_item->getExtras();
        if ( ! empty( $extras ) ) {
            foreach ( Lib\ProxyProviders\Local::findByIds( array_keys( $extras ) ) as $extra ) {
                $titles[] = Lib\Utils\Common::formatTitle( $extra->getTranslatedTitle(), $extras[ $extra->getId() ] );
            }
        }
        $data['extras'][] = implode( ', ', $titles );

        return $data;
    }

    /**
     * @inheritdoc
     */
    public static function prepareInfoTextCodes( array $info_text_codes, array $data )
    {
        $info_text_codes['{extras}'] = '<b>' . implode( ', ', $data['extras'] ) . '</b>';

        return $info_text_codes;
    }

    /**
     * @inheritdoc
     */
    public static function prepareCartItemInfoText( $data, BooklyLib\CartItem $cart_item )
    {
        if ( Lib\Plugin::enabled() ) {
            $titles = array();
            $extras = $cart_item->getExtras();
            if ( ! empty( $extras ) ) {
                foreach ( Lib\ProxyProviders\Local::findByIds( array_keys( $extras ) ) as $extra ) {
                    $titles[] = Lib\Utils\Common::formatTitle( $extra->getTranslatedTitle(), $extras[ $extra->getId() ] );
                }
            }
            $data['extras'][] = implode( ', ', $titles );
        }
        return $data;
    }

    /**
     * @inheritdoc
     */
    public static function renderCartItemInfo( BooklyLib\UserBookingData $userData, $cart_key, $positions, $desktop )
    {
        if ( get_option( 'bookly_service_extras_show_in_cart' ) ) {
            $cart_items = $userData->cart->getItems();
            $cart_item  = $cart_items[ $cart_key ];
            $template   = $desktop ? 'cart_extras' : 'cart_extras_mobile';
            $data       = array();
            $nop        = $cart_item->getNumberOfPersons();
            $extras_qty = $cart_item->getExtras();

            foreach ( BooklyLib\Proxy\ServiceExtras::findByIds( array_keys( $extras_qty ) ) as $extras ) {
                $quantity = $extras_qty[ $extras->getId() ];
                $data[]   = array(
                    'title'    => $extras->getTranslatedTitle(),
                    'quantity' => $quantity,
                    'price'    => BooklyLib\Utils\Price::format( $extras->getPrice() ),
                    'total'    => BooklyLib\Utils\Price::format( get_option( 'bookly_service_extras_multiply_nop' ) ? $nop * $quantity * $extras->getPrice() : $quantity * $extras->getPrice() ),
                );
            }

            self::renderTemplate( $template, compact( 'data', 'nop', 'positions', 'cart_key' ) );
        }
    }
}