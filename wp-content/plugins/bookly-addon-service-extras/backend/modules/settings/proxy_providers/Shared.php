<?php
namespace BooklyServiceExtras\Backend\Modules\Settings\ProxyProviders;

use Bookly\Backend\Modules\Settings\Proxy;

/**
 * Class Shared
 * @package BooklyServiceExtras\Backend\Modules\Settings\ProxyProviders
 */
class Shared extends Proxy\Shared
{
    /**
     * @inheritdoc
     */
    public static function renderSettingsMenu()
    {
        self::renderTemplate( 'settings_menu' );
    }

    /**
     * @inheritdoc
     */
    public static function renderSettingsForm()
    {
        self::renderTemplate( 'settings_form' );
    }

    /**
     * @inheritdoc
     */
    public static function renderCartSettings()
    {
        self::renderTemplate( 'cart' );
    }

    /**
     * @inheritdoc
     */
    public static function prepareCalendarAppointmentCodes( array $codes, $participants )
    {
        if ( $participants == 'one' ) {
            $codes[] = array( 'code' => 'extras', 'description' => __( 'extras titles', 'bookly-service-extras' ), );
            $codes[] = array( 'code' => 'extras_total_price', 'description' => __( 'extras total price', 'bookly-service-extras' ), );
        }

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public static function prepareWooCommerceCodes( array $codes )
    {
        $codes[] = array( 'code' => 'extras', 'description' => __( 'extras titles', 'bookly-service-extras' ) );

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public static function saveSettings( array $alert, $tab, $_post )
    {
        if ( $tab == 'service_extras' && ! empty( $_post ) ) {
            if ( ! array_key_exists( 'bookly_service_extras_show', $_post ) ) {
                $_post['bookly_service_extras_show'] = array();
            }
            $options = array( 'bookly_service_extras_enabled', 'bookly_service_extras_multiply_nop', 'bookly_service_extras_show' );
            foreach ( $options as $option_name ) {
                if ( array_key_exists( $option_name, $_post ) ) {
                    update_option( $option_name, $_post[ $option_name ] );
                }
            }
            $alert['success'][] = __( 'Settings saved.', 'bookly' );
        } else if ( $tab  == 'cart' ) {
            update_option( 'bookly_service_extras_show_in_cart', (int) $_post['bookly_service_extras_show_in_cart'] );
        }

        return $alert;
    }
}