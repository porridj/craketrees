<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
use Bookly\Backend\Components\Controls\Inputs;
use Bookly\Backend\Components\Settings\Selects;
use Bookly\Lib as BooklyLib;
?>
<div class="tab-pane" id="bookly_settings_service_extras">
    <form method="post" action="<?php echo esc_url( add_query_arg( 'tab', 'service_extras' ) ) ?>">
        <?php Selects::renderSingle( 'bookly_service_extras_enabled', __( 'Extras', 'bookly-service-extras' ), __( 'This setting enables or disables the Extras step of booking. You can choose what information should be displayed to your clients by using the checkboxes below.', 'bookly-service-extras' ) ) ?>
        <?php if ( BooklyLib\Config::groupBookingEnabled() ) : ?>
        <?php Selects::renderSingle( 'bookly_service_extras_multiply_nop', __( 'Multiply extras by number of persons', 'bookly-service-extras' ), __( 'If enabled, all extras will be multiplied by number of persons.', 'bookly-service-extras' ) ) ?>
        <?php endif; ?>
        <?php Selects::renderMultiple( 'bookly_service_extras_show', __( 'Show', 'bookly-service-extras' ), null, array( array( 'title', __( 'Title', 'bookly-service-extras' ) ), array( 'price', __( 'Price', 'bookly-service-extras' ) ), 'image' => array( 'image', __( 'Image', 'bookly-service-extras' ) ), 'duration' => array( 'duration', __( 'Duration', 'bookly-service-extras' ) ), 'summary' => array( 'summary', __( 'Summary', 'bookly-service-extras' ) ) ) ) ?>
        <div class="panel-footer">
            <?php Inputs::renderCsrf() ?>
            <?php Buttons::renderSubmit() ?>
            <?php Buttons::renderReset() ?>
        </div>
    </form>
</div>