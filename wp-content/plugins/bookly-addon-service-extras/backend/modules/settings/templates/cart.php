<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Settings\Selects;

Selects::renderSingle( 'bookly_service_extras_show_in_cart', __( 'Show extras', 'bookly-service-extras' ), __( 'If this setting is enabled then Bookly shows extras info for each service.', 'bookly-service-extras' ) );