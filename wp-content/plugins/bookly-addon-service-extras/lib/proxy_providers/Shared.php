<?php
namespace BooklyServiceExtras\Lib\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Lib\NotificationCodes;
use Bookly\Lib\Utils\Price;
use BooklyServiceExtras\Lib;

/**
 * Class Shared
 * @package BooklyServiceExtras\Lib\ProxyProviders
 */
class Shared extends BooklyLib\Proxy\Shared
{
    /**
     * Prepare data for notification codes.
     *
     * @param NotificationCodes $codes
     */
    public static function prepareNotificationCodesForOrder( NotificationCodes $codes )
    {
        $extras = array();
        $titles = array();
        $price  = 0.0;
        $item   = $codes->getItem();

        if ( $item->isCompound() ) {
            /** @var \Bookly\Lib\DataHolders\Booking\Compound $item */
            foreach ( $item->getItems() as $simple ) {
                foreach ( (array) json_decode( $simple->getCA()->getExtras(), true ) as $extras_id => $count ) {
                    $extras[ $extras_id ] = $count;
                }
            }
        } else {
            $extras = (array) json_decode( $item->getCA()->getExtras(), true );
        }

        /** @var Lib\Entities\ServiceExtra $extra */
        foreach ( Local::findByIds( array_keys( $extras ) ) as $extra ) {
            $quantity = $extras[ $extra->getId() ];
            $titles[] = Lib\Utils\Common::formatTitle( $extra->getTranslatedTitle(), $quantity );
            $price   += $extra->getPrice() * $quantity;
        }
        $codes->extras             = implode( ', ', $titles );
        $codes->extras_total_price = $price;
    }

    /**
     * Prepare replacements for notification codes.
     *
     * @param array             $codes
     * @param NotificationCodes $notification_codes
     * @param string            $format
     * @return array
     */
    public static function prepareReplaceCodes( array $codes, NotificationCodes $notification_codes, $format )
    {
        $extras = $notification_codes->extras;
        if ( $format == 'text' ) {
            /** @see \BooklyServiceExtras\Lib\Utils\Common::formatTitle() */
            $extras = str_replace( '&nbsp;&times;&nbsp;', ' x ', $extras );
        }
        $codes['{extras}']             = $extras;
        $codes['{extras_total_price}'] = Price::format( $notification_codes->extras_total_price );

        return $codes;
    }

}