jQuery(document).ready(function(){
	
	jQuery('.rm_section h3').click(function(){		
		if(jQuery(this).parent().next('.rm_options').css('display')=='none')
			{	jQuery(this).removeClass('inactive');
				jQuery(this).addClass('active');
				jQuery(this).children('img').removeClass('inactive');
				jQuery(this).children('img').addClass('active');
			
			}
		else
			{	jQuery(this).removeClass('active');
				jQuery(this).addClass('inactive');		
				jQuery(this).children('img').removeClass('active');			
				jQuery(this).children('img').addClass('inactive');
			}
		
		jQuery(this).parent().next('.rm_options').slideToggle('slow');
	});
	
	var journalLoaded = false;
	
	// On Load	
	if( jQuery(".rm_instagram_moderation input:checked").val() == 'mobile' ){
		jQuery(".rm_input.wordpress-moderation").hide();
	} else {
		jQuery(".rm_input.wordpress-moderation").show();
		
		if( !journalLoaded ){
			jQuery('.instagram-journal-choose').instagramJournal({
				accessToken : adminToken,
				tag: adminTag,
				mode : adminMode
			});
		
			journalLoaded = true;
		}
	}
		
	// On Change
	jQuery(".rm_instagram_moderation input").click(function(){ 
		if( jQuery(this).val() == 'mobile' ){
			jQuery(".rm_input.wordpress-moderation").slideUp();
		} else {
			jQuery(".rm_input.wordpress-moderation").slideDown();
			
			if( !journalLoaded ){
				jQuery('.instagram-journal-choose').instagramJournal({
					accessToken : adminToken,
					tag: adminTag,
					mode : adminMode
				});
			
				journalLoaded = true;
			}
		}
	});
	
	// Delete Confirmation
	jQuery(".rm_opts .delete-journal").click(function(){
		var answer = confirm('Are you sure you want to delete all photos?');
		if( answer == true ){
			// delete...
		} else {
			return false;
		}
	});
	
	// SHORTCODE GENERATOR
	jQuery(".shortcode_button").click(function() {
				
		// Build
		var gen_shortcode = '';
  		gen_shortcode += '[instagram-journal';
		
		jQuery(".journal-meta-group:visible").not(".text").each(function(){
			var target = jQuery(this).find(".widefat");
			
			if( target.val() != "" ){
				var paramName = (target.attr("id")).replace("journal_","");
				var paramVal = target.val();
				
				gen_shortcode += " " + paramName + '="' + paramVal + '"'
			}
		});
				
		gen_shortcode += ']';
		// console.log(gen_shortcode);
		
		// Insert
		if(jQuery("textarea.wp-editor-area").css("display") == "none") {
        	tinyMCE.execCommand("mceInsertContent",false, gen_shortcode);
			
      	} else {
        	var original_text = jQuery("textarea.wp-editor-area").val();
        	jQuery("textarea.wp-editor-area").val(original_text + gen_shortcode);
      	}
		
		// Clear
		jQuery(".journal-meta-group.advanced").slideUp();
		jQuery(".journal-meta-group.interface").slideUp();
		jQuery(".journal-colors-group").slideUp();
	});
	
	// On Load
	jQuery("#journal_tag, #journal_location_id").parent().hide();
	
	// On Change
	jQuery("#journal_mode").change(function(){
		// Clear
		jQuery("#journal_user_id, #journal_tag, #journal_location_id").parent().hide();
		
		// Render
		if( jQuery(this).val() == "user" || jQuery(this).val() == "multiuser" ){
			jQuery("#journal_user_id").parent().slideDown();
		}
		
		if( jQuery(this).val() == "tag" || jQuery(this).val() == "multitag" ){
			jQuery("#journal_tag").parent().slideDown();
		}
		
		if( jQuery(this).val() == "location" ){
			jQuery("#journal_location_id").parent().slideDown();
		}		
	});
	
	// Settings Toggle
	jQuery(".journal-advanced-btn").click(function(){
		jQuery(".journal-meta-group.advanced").slideToggle();
	});
	
	jQuery(".journal-interface-btn").click(function(){
		jQuery(".journal-meta-group.interface").slideToggle();
	});
	
	jQuery(".journal-colors-btn").click(function(){
		jQuery(".journal-colors-group").slideToggle();
	});
	
	// ColorPicker
	if( jQuery.fn.wpColorPicker ){ // Wordpress 3.4 and lower support
		jQuery('.journal-colorpicker').wpColorPicker();
	}
	
	// Color Group Toggle
	jQuery(".journal-colors-group .journal-meta-group").not(".classic-only, .all").hide();
	jQuery(".journal-meta-group #journal_gallery_mode").change(function(){
		jQuery(".journal-colors-group .journal-meta-group").not(".all").slideUp();
		jQuery(".journal-colors-group ."+jQuery(this).val()+"-only").slideDown();
	});
});