<?php 
/*
 * Plugin Name: Instagram Journal
 * Plugin URI: http://codecanyon.net/item/instagram-journal/11593035
 *
 * Author: Chris Rivers
 * Author URI: http://codecanyon.net/user/xxcriversxx
 *
 * Description: Capture and share the world's moments with Instagram Journal. A powerful, versatile plugin that allows you to display your Instagram photos on a website or blog-- leveraging the full power of the Instagram API.
 *
 * Version: 2.7
 *
 */

/* Globals 
------------------------*/
$isjournalaccesstoken = get_option('isjournal_accesstoken');
$isjournalcontesttag = get_option('isjournal_contest_tag');
$isjournalcontestmode = get_option('isjournal_contest_mode');
$isjournalcontestgallerymode = get_option('isjournal_contest_gallery_mode');

/* Admin Setup
------------------------*/
function instagram_journal_admin(){
	include('instagram_journal_admin.php');
}

function instagram_journal_actions(){
	add_menu_page( __( 'Instagram Journal', 'instagramJournal' ), __( 'Instagram Journal', 'instagramJournal' ),
		'edit_posts', 'instagramJournal', 'instagram_journal_admin',
		'../wp-content/plugins/instagram-journal/code/css/images/icon.png' );
}
add_action('admin_menu', 'instagram_journal_actions');

/* Load Libraries
------------------------*/
function instagram_journal_jquery_init() {
    if(!is_admin()) {
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'instagram_journal_jquery_init');

/* Load Front End Libraries
---------------------------------*/
function instagram_journal_add_to_header()
{
	$pathToTheme = get_bloginfo('wpurl') . "/wp-content/plugins/instagram-journal";
	
	// CSS	
	wp_enqueue_style('journal-style-1', $pathToTheme . '/jquery-instagram-journal/lib/fancybox2/source/jquery.fancybox.css');
	wp_enqueue_style('journal-style-2', $pathToTheme . '/jquery-instagram-journal/lib/jquery.jcarousel.css');
	wp_enqueue_style('journal-style-3', $pathToTheme . '/jquery-instagram-journal/instagramJournal.css');
	
	// Worpress Only CSS
	wp_enqueue_style('journal-style-4', $pathToTheme . '/code/css/wordpress.css');
	
	// HTML5 Video
	wp_enqueue_script('journal-script-1', $pathToTheme . '/jquery-instagram-journal/lib/videoplayer/js/mediaelement-and-player.min.js');
	wp_enqueue_style('journal-style-5', $pathToTheme . '/jquery-instagram-journal/lib/videoplayer/css/style.css');
		
	// JS
	wp_enqueue_script('journal-script-2', $pathToTheme . '/jquery-instagram-journal/lib/fancybox2/source/jquery.fancybox.pack.js');
	wp_enqueue_script('journal-script-3', $pathToTheme . '/jquery-instagram-journal/lib/dateFormat.js');
	wp_enqueue_script('journal-script-4', $pathToTheme . '/jquery-instagram-journal/lib/jquery.jcarousel.min.js');
	wp_enqueue_script('journal-script-5', $pathToTheme . '/jquery-instagram-journal/lib/easing.js');
	wp_enqueue_script('journal-script-6', $pathToTheme . '/jquery-instagram-journal/instagramJournal.js');
	wp_enqueue_script('journal-script-7', $pathToTheme . '/jquery-instagram-journal/lib/videoplayer/js/mediaelement-and-player.min.js');
}
add_action('wp_head', 'instagram_journal_add_to_header');

/* Load Back End Libraries
---------------------------------*/
function instagram_journal_admin_add_to_header()
{
	$pathToTheme = get_bloginfo('wpurl') . "/wp-content/plugins/instagram-journal";
	
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_style('admin-journal-style-1', $pathToTheme . '/code/functions/functions.css?ver=1.0');
	
	wp_enqueue_script('admin-journal-moderation-script', $pathToTheme . '/code/functions/instagramJournalModerate.js?ver=1.0');
	wp_enqueue_script('admin-journal-script-1', $pathToTheme . '/code/functions/rm_script.js?ver=1.0', array( 'wp-color-picker' ));
	
	global $isjournalaccesstoken;
	global $isjournalcontesttag;
	
	?>
	
	<script type="text/javascript">
		var adminToken = '<?php echo $isjournalaccesstoken; ?>';
		var adminTag = '<?php echo $isjournalcontesttag; ?>';
		var adminMode = 'tag';
	</script>
	
	<?php
}

add_action('admin_head', 'instagram_journal_admin_add_to_header');


/* Back End Ajax
-----------------------------------*/
add_action( 'admin_footer', 'save_photo_javascript' );

function save_photo_javascript(){
		
	// Get Base URL
	$journalAdminURL = admin_url().'admin.php?page=instagramJournal'; 
	?>
	
	<script type="text/javascript">
	
		jQuery(document).ready(function($){

			jQuery(".rm_section .new-photos .add-photos-btn").click(function(){
				
				// Store
				var data = { 'action': 'save_photo', 'entries' : [] };
				var totalApproved = jQuery(".approve-checkbox:checked").size();
				
				// Extract
				jQuery(".approve-checkbox:checked").each(function(i,e){

					data.entries[i] = {
						'id': jQuery(this).parent().attr("id"),
						'name': jQuery(this).parent().attr("data-name"),
						'author': jQuery(this).parent().attr("data-author"),
						'image': jQuery(this).parent().find("img").attr("src"),
						'video' : jQuery(this).parent().attr("data-video"),
						'link': jQuery(this).parent().attr("data-href"),
						'likes': jQuery(this).parent().attr("data-likes"),
						'date': jQuery(this).parent().attr("data-created"),
						'caption': jQuery(this).parent().attr("data-caption-std")										
					};
					
					// Ajax Call
					if( (i + 1) >= totalApproved ){
						// Note - ajaxurl is always defined in the admin header
						$.post(ajaxurl, data, function(response) {
														
							// Parse JSON
							var data = jQuery.parseJSON(response);
							
							// Clear UI
							jQuery(".rm_opts .current-photos ul li").not(".clear").remove();
							
							// Update UI
							jQuery.each(data,function(index,entryObj){
								var newEntry = '';
								newEntry += '<li>';
									newEntry += '<img src="'+entryObj['image']+'">';
									newEntry += '<a href="<?php echo $journalAdminURL; ?>&deleteContestEntry='+entryObj['id']+'">Delete</a>';
									newEntry += '<span>'+entryObj['likes']+'</span>';
								newEntry += '</li>';
								
								// Add New
								jQuery(newEntry).insertBefore(".rm_opts .current-photos ul li.clear");
							});
							
							// Scroll Up & Notification
							jQuery('html,body').animate({scrollTop: jQuery('body').offset().top - 100 }, 700);
							jQuery('.contest-updated').delay(700).fadeIn().delay(3000).fadeOut();
							
						});
					}
					
					// Uncheck Box
					$(this).attr('checked', false);
				
				});
				
				// Fallback
				if( totalApproved == 0 ){
					alert("Please select a photo before saving.");
				}
						
				return false;
			});
		
		});
		
	</script> <?php
		
}

// SAVE
add_action( 'wp_ajax_save_photo', 'save_photo_callback' );

function isJournalSortEntries (array $a, array $b) { 
	return $b["likes"] - $a["likes"]; 
}

function save_photo_callback(){
	
	// Get
	$newEntries = $_POST['entries'];
	$currentEntries = get_option('isjournal_contest_entries');
		
	// Store
	if( $currentEntries ){
		foreach( $newEntries as $newEntry ){
			// Clean
			$newEntry['name'] = instagram_journal_removeEmoji($newEntry['name']);
			$newEntry['author'] = instagram_journal_removeEmoji($newEntry['author']);
			$newEntry['caption'] = instagram_journal_removeEmoji($newEntry['caption']);
			
			// Combine & Conquer
			$currentEntries[$newEntry['id']] = $newEntry;
		}
		
		// Reorder & Save
		uasort($currentEntries, "isJournalSortEntries");
		update_option('isjournal_contest_entries', $currentEntries);
	
	} else {
		foreach( $newEntries as $key=>$newEntry ){
			// Clean
			$newEntry['name'] = instagram_journal_removeEmoji($newEntry['name']);
			$newEntry['author'] = instagram_journal_removeEmoji($newEntry['author']);
			$newEntry['caption'] = instagram_journal_removeEmoji($newEntry['caption']);
			
			// Format
			$newEntries[$newEntry['id']] = $newEntry;
			unset($newEntries[$key]);
		}
		
		// Reorder & Save
		uasort($newEntries, "isJournalSortEntries");
		update_option('isjournal_contest_entries', $newEntries);
	}
	
	// Send Response
	echo json_encode(get_option('isjournal_contest_entries'));

	// this is required to terminate immediately and return a proper response
	wp_die();
}

// DELETE
add_action('admin_init', 'delete_photo_callback');

function delete_photo_callback(){
	
	if( isset($_GET['deleteContestEntry']) && $_GET['deleteContestEntry'] != false ){
		// Get Options
		$contestEntries = get_option('isjournal_contest_entries');
		
		foreach( $contestEntries as $key=>$contestEntry ){
			// Remove Photo
			if( $key == $_GET['deleteContestEntry'] ){
				unset($contestEntries[$key]);
			}
		}

		// Update Option
		update_option('isjournal_contest_entries', $contestEntries);
		
		// Relocate
		header("Location: ".admin_url().'admin.php?page=instagramJournal');
	}
	
	if( isset($_GET['deleteJournal']) && $_GET['deleteJournal'] != false ){
		// Bye Bye Journal...
		delete_option('isjournal_contest_entries');
		
		// Relocate
		header("Location: ".admin_url().'admin.php?page=instagramJournal');
	}
	
}

// UPDATE
add_action('admin_init', 'update_photo_callback');

function update_photo_callback(){
	
	if( isset($_GET['updateContestEntry']) && $_GET['updateContestEntry'] != false ){
		// Get Options
		$contestEntries = get_option('isjournal_contest_entries');
		$token = get_option('isjournal_accesstoken');
		
		// Get Update Photo Likes
		foreach( $contestEntries as $key=>$contestEntry ){			
			// Connect Instagram
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://api.instagram.com/v1/media/'.$key.'?access_token='.$token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			$result = curl_exec($ch);
			$result = json_decode($result);
			curl_close($ch); 
			
			// Update Likes - Fallback if photo deleted...
			if( $result->data->likes->count ){
				$contestEntries[$key]['likes'] = $result->data->likes->count;
			}
		}
				
		// Update Option
		update_option('isjournal_contest_entries', $contestEntries);
	
		// Relocate
		header("Location: ".admin_url().'admin.php?page=instagramJournal');
	}
	
}


/* Instagram Journal Shortcode
-----------------------------------*/
function instagram_journal_shortcode($atts, $content = null) {
	
	STATIC $shortcodeID = 0;
	$shortcodeID++;	
	$parentID = '#instagram-journal-' . $shortcodeID;
	
	// Start Buffer
	ob_start();
	
	global $isjournalaccesstoken;
	
	$atts = shortcode_atts(array(
		// GENERAL
		"access_token" => $isjournalaccesstoken,
		"mode" => 'user',
		"user_id" => '1138644', 
		"tag" => 'sports',
		"location_id" => '230548',
		"gallery_mode" => 'classic',
		"video" => "false", 
		"speed" => 700, 
		"delay_interval" => 80, 
		"show_video_controls" => "false", 
		"carousel_autostart" => "true", 
		"carousel_scroll" => 1, 
		"carousel_easing" => 'linear',
		"carousel_duration" => 500, 
		"carousel_navigation" => "true",
		"count" => 20, 
		"limit" => 20,
		"large_carousel_width" => "60%",
		"is_full_width" => "false",
		"enable_links" => "true",
		"enable_fancybox" => "false",
		"gallery_size" => "auto",
		"photo_size" => "25%",
		"gutter_size" => "0",
		'background' => 'none',
		'classic_hover_photo_background' => '#3dc0f1',
		'classic_hover_video_background' => '#cc0000',
		'classic_hover_heading_color' => '#fff',
		'classic_hover_subheading_color' => '#fff',
		'classic_active_username_color' => '#535353',
		'classic_active_content_color' => '#939393',
		'classic_active_link_color' => '#3dc0f1',
		'collage_hover_background' => '',
		'collage_hover_heading_color' => '#155399',
		'collage_hover_subheading_color' => '#333',
		'collage_hover_button_color' => '#155399',
		'section_hover_background' => '#000',
		'section_hover_button_color' => '#fff',
		'section_heading_color' => '#535353',
		'section_content_color' => '#939393',
		'section_date_background' => '#155399',
		'section_date_color' => '#fff',
		'section_meta_color' => '#7cace2',
		'large_heading_color' => '#000',
		'large_content_color' => '#000',
		'large_strong_color' => '#b5b5b5',
		'large_link_color' => '#ff6f5a',
		'infinity_hover_background' => '#000',
		'infinity_hover_heading_color' => '#fff',
		'infinity_hover_date_color' => '#7e7e7e',
		'infinity_hover_meta_color' => '#3dc0f1',
		'infinity_hover_content_color' => '#c3c3c3',
		"filter" => ""
	), $atts);
	extract($atts);
	?>
		
	<?php if( $atts["gallery_mode"] == 'classic' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){								
				// Calling the Plugin
				jQuery('#<?php echo 'instagram-journal-' . $shortcodeID; ?>').instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
			<?php echo $parentID . ' .instagram-photo'; ?> { 
				background:<?php echo $classic_hover_photo_background; ?>;
				margin:<?php echo $gutter_size; ?>; 
				padding-bottom: <?php echo $photo_size; ?>; 
				width:<?php echo $photo_size; ?>; 
			}
			
			<?php echo $parentID . ' .instagram-photo.video'; ?> { background:<?php echo $classic_hover_video_background; ?>; }
			<?php echo $parentID . ' .instagram-photo .journal-meta'; ?> { color:<?php echo $classic_hover_heading_color; ?>; }
			<?php echo $parentID . ' .instagram-photo .journal-meta span'; ?> { color:<?php echo $classic_hover_subheading_color; ?>; }
			<?php echo $parentID . ' .instagram-content h1'; ?> { color:<?php echo $classic_active_username_color; ?>; }
			<?php echo $parentID . ' .instagram-content p'; ?> { color:<?php echo $classic_active_content_color; ?>; }
			<?php echo $parentID . ' .instagram-content p a'; ?> { color:<?php echo $classic_active_link_color; ?>; }
			<?php echo $parentID . ' .instagram-content ul li'; ?> { color:<?php echo $classic_active_content_color; ?>; }
		</style>
					
		<div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="classic-view clearfix <?php if( $atts['is_full_width'] == 'false' ){ echo 'classic-default'; } ?>">
			<div class="instagram-stage">
				<div class="instagram-media"></div>
				<div class="instagram-content"></div>
				<div class="clear"></div>
			</div>
		
			<!-- INSTAGRAM LOADING ZONE -->
		</div>
	<?php } ?>
		
	<?php if( $atts["gallery_mode"] == 'collage' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				// Collage View
				jQuery("#<?php echo 'instagram-journal-' . $shortcodeID; ?>").instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
			<?php echo $parentID . ' .instagram-photo'; ?> { background:<?php echo $collage_hover_background; ?>; }
			<?php echo $parentID . ' .overlay'; ?> { 
				<?php if( $collage_hover_background != "" ){ echo 'background:'.$collage_hover_background.';'; } ?> 
				<?php echo 'color:'.$collage_hover_heading_color; ?>;
			}
			
			<?php echo $parentID . ' .overlay .subtitle'; ?> { color:<?php echo $collage_hover_subheading_color; ?>; }
			<?php echo $parentID . ' .overlay .btn'; ?> { background:<?php echo $collage_hover_button_color; ?>; }
		</style>
		
		<div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="collage-view clearfix">
			<div class="jcarousel-wrapper">
				
				<?php if( $atts["carousel_navigation"] == "true" ){ ?>
				<div class="controls">
					<a href="#" class="jcarousel-control-prev"></a>
	            	<a href="#" class="jcarousel-control-next"></a>
				</div>
				<?php } ?>
			
				<div class="jcarousel large">
					<ul></ul>
				</div>
			
				<div class="jcarousel med">
	                <ul></ul>
				</div>
			</div>
	
			<div class="clear"></div>
		</div>
	<?php } ?>
		
	<?php if( $atts["gallery_mode"] == 'section' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				// Section View
				jQuery("#<?php echo 'instagram-journal-' . $shortcodeID; ?>").instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
			<?php echo $parentID . ' .instagram-photo'; ?> { background-color:<?php echo $section_hover_background; ?>; }
			<?php echo $parentID . ' .instagram-photo .view-photo'; ?> { 
				border-color:<?php echo $section_hover_button_color; ?>; 
				color:<?php echo $section_hover_button_color; ?>; 
			}
			
			<?php echo $parentID . ' .post-text h4'; ?> { color:<?php echo $section_heading_color; ?>; ?>; }
			<?php echo $parentID . ' .post-text p'; ?> { color:<?php echo $section_content_color; ?>; }
			<?php echo $parentID . ' .post-meta'; ?> { background:<?php echo $section_date_background; ?>; }
			<?php echo $parentID . ' .post-meta h5'; ?> { color:<?php echo $section_date_color; ?>; }
			<?php echo $parentID . ' .post-meta .meta'; ?> { color:<?php echo $section_meta_color; ?>; }
		</style>
		
	    <div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="carousel-view jcarousel-wrapper">
			<?php if( $atts["carousel_navigation"] == "true" ){ ?>
			<div class="controls">
				<a href="#" class="jcarousel-control-prev"></a>
	        	<a href="#" class="jcarousel-control-next"></a>
			</div>
			<?php } ?>

	        <div class="jcarousel">
	            <ul></ul>
	        </div>
	    </div>
	<?php } ?>
		
	<?php if( $atts["gallery_mode"] == 'large' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#<?php echo 'instagram-journal-' . $shortcodeID; ?>").instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
			<?php echo $parentID . ' .navigator h2'; ?> { color:<?php echo $large_heading_color; ?>; }
			<?php echo $parentID . ' .navigator p'; ?> { color:<?php echo $large_content_color; ?>; }
			<?php echo $parentID . ' .navigator li'; ?> { color:<?php echo $large_content_color; ?>; }
			<?php echo $parentID . ' .navigator li strong'; ?> { color:<?php echo $large_strong_color; ?>; }
			<?php echo $parentID . ' .navigator a'; ?> { color:<?php echo $large_link_color; ?>; }
		</style>
		
		<div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="large-view">
		
			<div class="navigator"></div>	
		
			<div class="jcarousel-wrapper">
				<?php if( $atts["carousel_navigation"] == "true" ){ ?>
				<div class="controls">
					<a href="#" class="jcarousel-control-prev"></a>
	            	<a href="#" class="jcarousel-control-next"></a>
				</div>
				<?php } ?>

	            <div class="jcarousel" style="width:<?php echo $atts['large_carousel_width']; ?>">
					<ul></ul>
	            </div>
	        </div>
		</div>
	<?php } ?>	
		
	<?php if( $atts["gallery_mode"] == 'infinity' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#<?php echo 'instagram-journal-' . $shortcodeID; ?>").instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
			
			<?php echo $parentID . ' .instagram-photo'; ?> { background:<?php echo $infinity_hover_background; ?>; }
			<?php echo $parentID . ' .profile h3'; ?> { color:<?php echo $infinity_hover_heading_color; ?>; }
			<?php echo $parentID . ' .profile em'; ?> { color:<?php echo $infinity_hover_date_color; ?>; }
			<?php echo $parentID . ' .profile span'; ?> { color:<?php echo $infinity_hover_meta_color; ?>; }
			<?php echo $parentID . ' .profile p'; ?> { color:<?php echo $infinity_hover_content_color; ?>; }
		</style>
		
		<div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="infinity-view">				
			<div class="jcarousel-wrapper">
				
				<?php if( $atts["carousel_autostart"] == "false" && $atts["carousel_navigation"] == "true" ){ ?>
				<div class="controls">
					<a href="#" class="jcarousel-control-prev"></a>
	            	<a href="#" class="jcarousel-control-next"></a>
				</div>
				<?php } ?>

	            <div class="jcarousel">
					<ul></ul>
	            </div>
	        </div>
		</div>
	<?php } ?>
	
	<?php if( $atts["gallery_mode"] == 'contest' ){ ?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery("#<?php echo 'instagram-journal-' . $shortcodeID; ?>").instagramJournal({
					accessToken : '<?php echo $atts["access_token"]; ?>',
					mode : '<?php echo $atts["mode"]; ?>',
					userID : '<?php echo $atts["user_id"]; ?>',
					tag: '<?php echo $atts["tag"]; ?>',
					locationID: '<?php echo $atts["location_id"]; ?>',
					galleryMode: '<?php echo $atts["gallery_mode"]; ?>',
					video: <?php echo $atts["video"]; ?>,
					speed: <?php echo $atts["speed"]; ?>,
					delayInterval: <?php echo $atts["delay_interval"]; ?>,
					showVideoControls: <?php echo $atts["show_video_controls"]; ?>,
					carouselAutostart: <?php echo $atts["carousel_autostart"]; ?>,
					carouselScroll: <?php echo $atts["carousel_scroll"]; ?>, 
					carouselEasing: '<?php echo $atts["carousel_easing"]; ?>',
					carouselDuration: <?php echo $atts["carousel_duration"]; ?>,
					count: <?php echo $atts["count"]; ?>,
					limit: <?php echo $atts["limit"]; ?>,
					enableLinks: <?php echo $atts["enable_links"]; ?>,
					enableFancybox: <?php echo $atts["enable_fancybox"]; ?>,
					filter: '<?php echo $atts["filter"]; ?>'
				});
			});
		</script>
			
		<style type="text/css">
			<?php echo $parentID; ?> { background:<?php echo $background; ?>; width:<?php echo $gallery_size; ?>; }
		</style>
		
		<div id="<?php echo 'instagram-journal-' . $shortcodeID; ?>" class="contest-view content">
			<div class="contest-collection"></div>
		</div>
	<?php } ?>	
			
	<?php	
	$output = ob_get_clean();
	return $output;
}
add_shortcode('instagram-journal','instagram_journal_shortcode');

/* Instagram Contest Shortcode
-----------------------------------*/
function instagram_journal_contest_shortcode($atts, $content = null) {
	
	// Start Buffer
	ob_start();
	
	global $isjournalaccesstoken;
	global $isjournalcontesttag;
	global $isjournalcontestmode;
	global $isjournalcontestgallerymode;
	
	
	$atts = shortcode_atts(array(
		"access_token" => $isjournalaccesstoken,
		"tag" => $isjournalcontesttag,
		"gallery_mode" => $isjournalcontestgallerymode,
		"is_full_width" => "false"
	), $atts);
	extract($atts);
	
	// Mobile Moderation Mode	
	if( $isjournalcontestmode == "mobile" ){ ?>
		
		<?php if( $atts["gallery_mode"] == 'classic' ){ ?>
			<script type="text/javascript">
				jQuery(document).ready(function(){								
					// Calling the Plugin
					jQuery('.classic-view').instagramJournal({
						accessToken : '<?php echo $atts["access_token"]; ?>',
						mode : 'contest',
						tag: '<?php echo $atts["tag"]; ?>',
						galleryMode: '<?php echo $atts["gallery_mode"]; ?>'
					});
				});
			</script>
					
			<div class="classic-view clearfix <?php if( $atts['is_full_width'] == 'false' ){ echo 'classic-default'; } ?>">
				<div class="instagram-stage">
					<div class="instagram-media"></div>
					<div class="instagram-content"></div>
					<div class="clear"></div>
				</div>
		
				<!-- INSTAGRAM LOADING ZONE -->
			</div>
		<?php } ?>	
	
		<?php if( $atts["gallery_mode"] == 'contest' ){ ?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery(".contest-view").instagramJournal({
						accessToken : '<?php echo $atts["access_token"]; ?>',
						mode : 'contest',
						tag: '<?php echo $atts["tag"]; ?>',
						galleryMode: '<?php echo $atts["gallery_mode"]; ?>'
					});
				});
			</script>
		
			<div class="contest-view content">
				<div class="contest-collection"></div>
			</div>
		<?php 
		
			}
			 
		} else { 
			
			// Wordpress Moderation Mode
			if( $atts["gallery_mode"] == 'classic' ){				
				?>
				
				<script type="text/javascript">
					jQuery(document).ready(function(){
						
						var contestSettings = [];
						contestSettings.video = true;
						contestSettings.showVideoControls = false;
						
						jQuery(document).on("click", ".classic-view .instagram-photo", function(){
							jQuery(".classic-view .instagram-photo").removeClass("active");
							jQuery(this).addClass("active");
			
							explorePhoto(jQuery(this));
							startVideoPlayer(contestSettings);
						
							return false;
						});
						
						jQuery(document).on("click", ".instagram-nav a.ps-close", function(){
							jQuery('.classic-view .instagram-stage').slideUp().removeClass('active');
						});
						
						jQuery(document).on("click", ".instagram-nav a.ps-next", function(){
							// Store Current/Next Photo
							var currentPhoto = jQuery(".classic-view .instagram-photo.active");
							var nextPhoto = jQuery(".classic-view .instagram-photo.active").next();
	
							if( nextPhoto.index() == -1 ){
								nextPhoto = jQuery(".classic-view .instagram-photo").first();
							}
	
							jQuery(".classic-view .instagram-media img, .classic-view .instagram-media video").fadeOut(function(){
								// Explore Photo
								explorePhoto(nextPhoto);
								startVideoPlayer(contestSettings);
							});

							// Update UI
							jQuery(".classic-view .instagram-photo").removeClass("active");
							jQuery(nextPhoto).addClass("active");
						});

						jQuery(document).on("click", ".instagram-nav a.ps-prev", function(){
							// Store Current/Next Photo
							var currentPhoto = jQuery(".classic-view .instagram-photo.active");
							var prevPhoto = jQuery(".classic-view .instagram-photo.active").prev();
	
							if( prevPhoto.index() == 0 ){
								prevPhoto = jQuery(".classic-view .instagram-photo").last();
							}
	
							jQuery(".classic-view .instagram-media img, .classic-view .instagram-media video").fadeOut(function(){
								// Explore Photo
								explorePhoto(prevPhoto);
								startVideoPlayer(contestSettings);
							});

							// Update UI
							jQuery(".classic-view .instagram-photo").removeClass("active");
							jQuery(prevPhoto).addClass("active");
						});
						
					});
				</script>
				
				<div class="classic-view clearfix <?php if( $atts['is_full_width'] == 'false' ){ echo 'classic-default'; } ?>">
					<div class="instagram-stage">
						<div class="instagram-media"></div>
						<div class="instagram-content"></div>
						<div class="clear"></div>
					</div>
		
					<?php
						// Get
						$currentEntries = get_option('isjournal_contest_entries');
						
						// Build
						foreach( $currentEntries as $currentEntry ){
							
							// Fallback
							if( $currentEntry['date'] == '' ){
								$currentEntry['date'] = '1435155663';
							}
							
							if( $currentEntry['author'] == '' ){
								$currentEntry['author'] = $currentEntry['name'];
							}
							
							if( $currentEntry['video'] ){
								echo '<a class="instagram-photo contest-photo video" href="'.$currentEntry['link'].'" data-created="'.$currentEntry['date'].'" data-name="'.$currentEntry['name'].'" data-author="'.$currentEntry['author'].'" data-likes="'.$currentEntry['likes'].'" data-caption="'.$currentEntry['caption'].' &lt;a target=&quot;_blank&quot; href=&quot;'.$currentEntry['link'].'&quot;&gt;View Photo&lt;/a&gt;" data-video="'.$currentEntry['video'].'">';
									
							} else {
								echo '<a class="instagram-photo contest-photo image" href="'.$currentEntry['link'].'" data-created="'.$currentEntry['date'].'" data-name="'.$currentEntry['name'].'" data-author="'.$currentEntry['author'].'" data-likes="'.$currentEntry['likes'].'" data-caption="'.$currentEntry['caption'].' &lt;a target=&quot;_blank&quot; href=&quot;'.$currentEntry['link'].'&quot;&gt;View Photo&lt;/a&gt;">';
							}	
														
							echo '	<img src="'.$currentEntry['image'].'">';
							echo '  <span class="likes">'.$currentEntry['likes'].'</span>';
							echo '</a>';
						}
					?>
				</div>
				<?php
			}
			
			if( $atts["gallery_mode"] == 'contest' ){
				// echo "contest";
				?>
				
				<div class="contest-view content">
					<div class="contest-collection">
						<?php
							// Get
							$currentEntries = get_option('isjournal_contest_entries');
						
							// Build
							foreach( $currentEntries as $currentEntry ){								
								echo '<a class="instagram-photo contest-photo" href="'.$currentEntry['link'].'" target="_blank">';
								echo '	<div class="instagram-media">';
								echo '		<img class="photo" src="'.$currentEntry['image'].'">';
								echo '	</div>';
								
								echo '	<div class="instagram-content">';
								echo '		<span class="author">'.$currentEntry['name'].'</span>';
								echo '		<span class="date">'.$isjournalcontesttag.'</span>';
								echo '		<span class="likes">'.$currentEntry['likes'].'</span>';
								echo '	</div>';
								echo '</a>';
							}
						?>
					</div>	
				</div>
				
				<?php
			}
			
		}
		?>
			
	<?php	
	$output = ob_get_clean();	
	return $output;
}
add_shortcode('instagram-journal-contest','instagram_journal_contest_shortcode');

function instagram_journal_removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);
    
    // Match flags (iOS)
    $regexTransport = '/[\x{1F1E0}-\x{1F1FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);
    

    return $clean_text;
}

/* Custom Meta Box
-----------------------------------*/
add_action( 'load-post.php', 'instagramjournal_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'instagramjournal_post_meta_boxes_setup' );

function instagramjournal_post_meta_boxes_setup() {
	add_action( 'add_meta_boxes', 'instagramjournal_add_post_meta_boxes' );
}

function instagramjournal_add_post_meta_boxes() {
	add_meta_box(
		'instagramjournal-post-class', // Unique ID
		esc_html__( 'Instagram Shortcode Options', 'xxcriversxx' ), // Title
		'instagramjournal_post_class_meta_box', // Callback function
		'page',	// Admin page (or post type)
		'side',	// Context
		'high' // Priority
	);
}

function instagramjournal_post_class_meta_box( $object, $box ) { ?>

	<?php wp_nonce_field( basename( __FILE__ ), 'instagramjournal_post_class_nonce' ); ?>
	<?php $currentPostClass = esc_attr( get_post_meta( $object->ID, 'instagramjournal_post_class', true )); ?>

	<div class="journal-meta-group text">
		<small><?php _e( "Select a your options from the list below, then click 'Insert Shortcode'.", 'instagramjournalexample' ); ?></small>
	</div>
	
	<div class="journal-meta-group">
		<strong>Mode</strong>
		<select id="journal_mode" class="widefat">
			<option value="contest">Contest</option>
			<option value="liked">Liked</option>
			<option value="location">Location</option>
			<option value="multitag">Multi Tag</option>
			<option value="multiuser">Multi User</option>
			<option value="tag">Tag</option>
			<option value="user" selected="selected">User</option>
		</select>
	</div>
	
	<div class="journal-meta-group">
		<strong>User ID <small>(example: "1138644")</small></strong>
		<input id="journal_user_id" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group">
		<strong>Tag <small>(example: "selfie")</small></strong>
		<input id="journal_tag" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group">
		<strong>Location ID <small>(example: "230548")</small></strong>
		<input id="journal_location_id" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group">
		<strong>Gallery Mode</strong>
		<select id="journal_gallery_mode" class="widefat">
			<option value="classic" selected="selected">Classic</option>
			<option value="collage">Collage</option>
			<option value="contest">Contest</option>
			<option value="infinity">Infinity Carousel</option>
			<option value="large">Large Carousel</option>
			<option value="section" >Section Carousel</option>
		</select>
	</div>
	
	<p><a class="journal-interface-btn journal-toggle">Size Settings</a></p>
	
	<div class="journal-meta-group interface">
		<strong>Gallery Size <small>(tip: use large or full for carousels)</small></strong>
		<select id="journal_gallery_size" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="auto">Default</option>
			<option value="100%">Full</option>
			<option value="75%">Large</option>
			<option value="50%">Half</option>
			<option value="25%">Quarter</option>
		</select>
	</div>
	
	<div class="journal-meta-group interface">
		<strong>Photo Size <small>(classic mode only)</small></strong>
		<select id="journal_photo_size" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="25%">Default</option>
			<option value="100%">100% (1x20)</option>
			<option value="50%">50% (2x10</option>
			<option value="33%">33% (3x7)</option>
			<option value="25%">25% (4x5)</option>
			<option value="20%">20% (5x4)</option>
			<option value="16.666%">16.666% (6x4)</option>
			<option value="14.28%">14.28% (7x3)</option>
			<option value="12.5%">12.5% (8x3)</option>
			<option value="11.11%">11.11% (9x3)</option>
			<option value="10%">10% (10x2)</option>
		</select>
	</div>
	
	<div class="journal-meta-group interface">
		<strong>Photo Gutter Size <small>(classic mode only)</small></strong>
		<select id="journal_gutter_size" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="0">Default</option>
			<option value="5px">5px</option>
			<option value="10px">10px</option>
			<option value="15px">15px</option>
			<option value="20px">20px</option>
			<option value="25px">25px</option>
			<option value="30px">30px</option>
			<option value="35px">35px</option>
			<option value="40px">40px</option>
			<option value="45px">45px</option>
			<option value="50px">50px</option>
		</select>
	</div>
	
	<p><a class="journal-colors-btn journal-toggle">Color Settings</a></p>
	
	<div class="journal-colors-group">
		<div class="journal-meta-group all">
			<strong>Background</strong><br />
			<input id="journal_background" type="text" value="" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Hover Photo Background <small>(classic only)</small></strong><br />
			<input id="journal_classic_hover_photo_background" type="text" value="#3dc0f1" class="journal-colorpicker widefat" />
		</div>
		
		<div class="journal-meta-group classic-only">
			<strong>Hover Video Background <small>(classic only)</small></strong><br />
			<input id="journal_classic_hover_video_background" type="text" value="#cc0000" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Hover Heading Color <small>(classic only)</small></strong><br />
			<input id="journal_classic_hover_heading_color" type="text" value="#fff" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Hover Sub Heading Color <small>(classic only)</small></strong><br />
			<input id="journal_classic_hover_subheading_color" type="text" value="#fff" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Active Username Color <small>(classic only)</small></strong><br />
			<input id="journal_classic_active_username_color" type="text" value="#535353" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Active Content Color <small>(classic only)</small></strong><br />
			<input id="journal_classic_active_content_color" type="text" value="#939393" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group classic-only">
			<strong>Active Link Color <small>(classic only)</small></strong><br />
			<input id="journal_classic_active_link_color" type="text" value="#3dc0f1" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group collage-only">
			<strong>Hover Background <small>(collage only)</small></strong><br />
			<input id="journal_collage_hover_background" type="text" value="" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group collage-only">
			<strong>Hover Heading Color <small>(collage only)</small></strong><br />
			<input id="journal_collage_hover_heading_color" type="text" value="#155399" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group collage-only">
			<strong>Hover Sub Heading Color <small>(collage only)</small></strong><br />
			<input id="journal_collage_hover_subheading_color" type="text" value="#333333" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group collage-only">
			<strong>Hover Button Color <small>(collage only)</small></strong><br />
			<input id="journal_collage_hover_button_color" type="text" value="#155399" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Hover Background <small>(section only)</small></strong><br />
			<input id="journal_section_hover_background" type="text" value="#000" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Hover Button Color <small>(section only)</small></strong><br />
			<input id="journal_section_hover_button_color" type="text" value="#fff" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Heading Color <small>(section only)</small></strong><br />
			<input id="journal_section_heading_color" type="text" value="#155399" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Content Color <small>(section only)</small></strong><br />
			<input id="journal_section_content_color" type="text" value="#155399" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Date Background <small>(section only)</small></strong><br />
			<input id="journal_section_date_background" type="text" value="#155399" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Date Color <small>(section only)</small></strong><br />
			<input id="journal_section_date_color" type="text" value="#fff" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group section-only">
			<strong>Likes/Comments Color <small>(section only)</small></strong><br />
			<input id="journal_section_meta_color" type="text" value="#7cace2" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group large-only">
			<strong>Heading Color <small>(large only)</small></strong><br />
			<input id="journal_large_heading_color" type="text" value="#000" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group large-only">
			<strong>Content Color <small>(large only)</small></strong><br />
			<input id="journal_large_content_color" type="text" value="#000" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group large-only">
			<strong>Strong Content Color <small>(large only)</small></strong><br />
			<input id="journal_large_strong_color" type="text" value="#b5b5b5" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group large-only">
			<strong>Link Color <small>(large only)</small></strong><br />
			<input id="journal_large_link_color" type="text" value="#ff6f5a" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group infinity-only">
			<strong>Hover Background <small>(infinity only)</small></strong><br />
			<input id="journal_infinity_hover_background" type="text" value="#000" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group infinity-only">
			<strong>Hover Heading Color <small>(infinity only)</small></strong><br />
			<input id="journal_infinity_hover_heading_color" type="text" value="#fff" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group infinity-only">
			<strong>Hover Date Color <small>(infinity only)</small></strong><br />
			<input id="journal_infinity_hover_date_color" type="text" value="#7e7e7e" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group infinity-only">
			<strong>Hover Meta Color <small>(infinity only)</small></strong><br />
			<input id="journal_infinity_hover_meta_color" type="text" value="#3dc0f1" class="journal-colorpicker widefat" />
		</div>
	
		<div class="journal-meta-group infinity-only">
			<strong>Hover Content Color <small>(infinity only)</small></strong><br />
			<input id="journal_infinity_hover_content_color" type="text" value="#c3c3c3" class="journal-colorpicker widefat" />
		</div>
	</div>
	
	<p><a class="journal-advanced-btn journal-toggle">Advanced Settings</a></p>
	
	<div class="journal-meta-group advanced">
		<strong>Video</strong>
		<select id="journal_video" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="false">Disabled</option>
			<option value="true">Enabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Show Video Controls</strong>
		<select id="journal_show_video_controls" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="false">Disabled</option>
			<option value="true">Enabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Speed <small>(example: "700")</small></strong>
		<input id="journal_speed" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Delay Interval <small>(example: "80")</small></strong>
		<input id="journal_delay_interval" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Carousel Autostart</strong>
		<select id="journal_carousel_autostart" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="false">Disabled</option>
			<option value="true">Enabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Carousel Scroll <small>(example: "1")</small></strong>
		<input id="journal_carousel_scroll" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Carousel Easing</strong>		
		<select id="journal_carousel_easing" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="linear">linear</option>
			<option value="swing">swing</option>
			<option value="easeInQuad">easeInQuad</option>
			<option value="easeOutQuad">easeOutQuad</option>
			<option value="easeInOutQuad">easeInOutQuad</option>
			<option value="easeInCubic">easeInCubic</option>
			<option value="easeOutCubic">easeOutCubic</option>
			<option value="easeInOutCubic">easeInOutCubic</option>
			<option value="easeInQuart">easeInQuart</option>
			<option value="easeOutQuart">easeOutQuart</option>
			<option value="easeInOutQuart">easeInOutQuart</option>
			<option value="easeInQuint">easeInQuint</option>
			<option value="easeOutQuint">easeOutQuint</option>
			<option value="easeInOutQuint">easeInOutQuint</option>
			<option value="easeInExpo">easeInExpo</option>
			<option value="easeOutExpo">easeOutExpo</option>
			<option value="easeInOutExpo">easeInOutExpo</option>
			<option value="easeInSine">easeInSine</option>
			<option value="easeOutSine">easeOutSine</option>
			<option value="easeInOutSine">easeInOutSine</option>
			<option value="easeInCirc">easeInCirc</option>
			<option value="easeOutCirc">easeOutCirc</option>
			<option value="easeInOutCirc">easeInOutCirc</option>
			<option value="easeInElastic">easeInElastic</option>
			<option value="easeOutElastic">easeOutElastic</option>
			<option value="easeInOutElastic">easeInOutElastic</option>
			<option value="easeInBack">easeInBack</option>
			<option value="easeOutBack">easeOutBack</option>
			<option value="easeInOutBack">easeInOutBack</option>
			<option value="easeInBounce">easeInBounce</option>
			<option value="easeOutBounce">easeOutBounce</option>
			<option value="easeInOutBounce">easeInOutBounce</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Carousel Duration <small>(example: "500")</small></strong>
		<input id="journal_carousel_duration" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Count <small>(example: "20")</small></strong>
		<input id="journal_count" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Limit <small>(example: "20")</small></strong>
		<input id="journal_limit" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Large Carousel Width <small>(example: "60%")</small></strong>
		<input id="journal_large_carousel_width" type="text" class="widefat" value="" />
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Is Full Width</strong>
		<select id="journal_is_full_width" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="false">Disabled</option>
			<option value="true">Enabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Enable Links</strong>
		<select id="journal_enable_links" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="true">Enabled</option>
			<option value="false">Disabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Enable Fancybox <small>(Classic Only)</small></strong>
		<select id="journal_enable_fancybox" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="true">Enabled</option>
			<option value="false">Disabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Carousel Navigation <small>(show or hide arrows)</small></strong>
		<select id="journal_carousel_navigation" class="widefat">
			<option value="" selected="selected">-- Please Choose --</option>
			<option value="false">Disabled</option>
			<option value="true">Enabled</option>
		</select>
	</div>
	
	<div class="journal-meta-group advanced">
		<strong>Filter <small>(example: "wordpress")</small></strong>
		<input id="journal_filter" type="text" class="widefat" value="" />
	</div>
	
	<input type="button" id="alert" name="alert-button" class="button-primary shortcode_button" value="Insert Shortcode">
<?php } ?>
