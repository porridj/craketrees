function instagramFetch(settings) {
    var access_token = settings.accessToken;
    var param = {
        access_token: access_token
    };
    fetchCMD(param, settings)
}

function fetchCMD(param, settings) {
    var cmdURL = "";
    if (settings.mode == 'user') {
        cmdURL = 'https://api.instagram.com/v1/users/' + settings.userID + '/media/recent/?callback=?'
    } else if (settings.mode == 'liked') {
        cmdURL = 'https://api.instagram.com/v1/users/self/media/liked?count=' + settings.count + '&callback=?'
    } else if (settings.mode == 'popular') {
        cmdURL = 'https://api.instagram.com/v1/media/popular?callback=?'
    } else if (settings.mode == 'location') {
        cmdURL = 'https://api.instagram.com/v1/locations/' + settings.locationID + '/media/recent/?callback=?'
    } else if (settings.mode == 'multitag') {
        var tagsArray = settings.tag.replace(/ /g, '');
        tagsArray = tagsArray.split(",");
        settings.multiCompleteTotal = tagsArray.length - 1;
        settings.multiCompleteCount = 0;
        jQuery.each(tagsArray, function(index) {
            cmdURL = 'https://api.instagram.com/v1/tags/' + tagsArray[index] + '/media/recent?callback=?';
            jQuery.getJSON(cmdURL, param, function(data) {
                onPhotoLoaded(data, settings, "off")
            })
        });
        return !1
    } else if (settings.mode == 'multiuser') {
        var userIDArray = settings.userID;
        userIDArray = userIDArray.split(",");
        settings.multiCompleteTotal = userIDArray.length - 1;
        settings.multiCompleteCount = 0;
        jQuery.each(userIDArray, function(index) {
            cmdURL = 'https://api.instagram.com/v1/users/' + userIDArray[index] + '/media/recent/?callback=?';
            jQuery.getJSON(cmdURL, param, function(data) {
                onPhotoLoaded(data, settings, "off")
            })
        });
        return !1
    } else {
        var tagQuery = settings.tag.replace(/ /g, '');
        cmdURL = 'https://api.instagram.com/v1/tags/' + tagQuery + '/media/recent?callback=?'
    }
    jQuery.getJSON(cmdURL, param, function(data) {
        onPhotoLoaded(data, settings)
    })
}

function instagramUserSearch(settings) {
    var access_token = settings.accessToken;
    var searchQuery = settings.user;
    var param = {
        access_token: access_token,
        q: searchQuery
    };
    userSearchCMD(param, settings)
}

function userSearchCMD(param, settings) {
    var cmdURL = 'https://api.instagram.com/v1/users/search?callback=?';
    jQuery.getJSON(cmdURL, param, function(data) {
        onUserLoaded(data, settings)
    })
}

function onUserLoaded(data, settings) {
    if (data.meta.code == 200) {
        var users = data.data;
        if (users.length > 0) {
            for (var key in users) {
                var user = users[key];
                var instagramUser = '';
                instagramUser = '<div class="instagram-user-all" id="p' + user.id + '" title="' + user.username + '" rel="' + user.id + '">';
                instagramUser += "<img src='" + user.profile_picture + "' />";
                instagramUser += '</div>';
                jQuery(instagramUser).appendTo(settings.element)
            }
            jQuery('<div class="clear"></div><span class="ig-glyph"></span>').appendTo(settings.element)
        } else {
            alert("No users found with this name.")
        }
    }
}

function instagramTagsLoadMore(settings) {
    var access_token = settings.accessToken;
    var param = {
        access_token: access_token,
        max_tag_id: settings.instagramBrowserNextMax
    };
    if (settings.mode == 'tag' || settings.mode == 'contest') {
        var searchQuery = settings.tag.replace(/ /g, '')
    }
    loadMoreCMD(settings, param, searchQuery)
}

function loadMoreCMD(settings, param, searchQuery) {
    var cmdURL = "https://api.instagram.com/v1/tags/" + searchQuery + "/media/recent?callback=?";
    jQuery.getJSON(cmdURL, param, function(data) {
        onPhotoLoaded(data, settings)
    })
}

function instagramUsersLoadMore(settings) {
    var access_token = settings.accessToken;
    var param = {
        access_token: access_token,
        max_id: settings.instagramBrowserNextMax
    };
    loadMoreUsersCMD(settings, param)
}

function loadMoreUsersCMD(settings, param) {
    var cmdURL = 'https://api.instagram.com/v1/users/' + settings.userID + '/media/recent/?callback=?';
    jQuery.getJSON(cmdURL, param, function(data) {
        onPhotoLoaded(data, settings)
    })
}

function instagramLocationLoadMore(settings) {
    var access_token = settings.accessToken;
    var param = {
        access_token: access_token,
        max_id: settings.instagramBrowserNextMax
    };
    loadMoreLocationsCMD(settings, param)
}

function loadMoreLocationsCMD(settings, param) {
    var cmdURL = 'https://api.instagram.com/v1/locations/' + settings.locationID + '/media/recent/?callback=?';
    jQuery.getJSON(cmdURL, param, function(data) {
        onPhotoLoaded(data, settings)
    })
}

function instagramLikedLoadMore(settings) {
    var access_token = settings.accessToken;
    var param = {
        access_token: access_token,
        max_like_id: settings.instagramBrowserNextMax
    };
    loadMoreLikedCMD(settings, param)
}

function loadMoreLikedCMD(settings, param) {
    var cmdURL = 'https://api.instagram.com/v1/users/self/media/liked?count=' + settings.count + '&callback=?';
    jQuery.getJSON(cmdURL, param, function(data) {
        onPhotoLoaded(data, settings)
    })
}

function explorePhoto(photo, selector) {
    var obDate = parseInt(photo.attr("data-created"));
    obDate = new Date(obDate * 1000);
    obDate = dateFormat(obDate, "dddd, mmmm dS, yyyy, h:MM TT");
    var stagecontent = '';
    stagecontent += '';
    stagecontent += '<h1>' + photo.attr("data-name") + '</h1>';
    stagecontent += '<p>' + photo.attr("data-caption") + '</p>';
    stagecontent += '';
    stagecontent += '<ul class="details">';
    stagecontent += '	<li><strong>Author</strong>: ' + photo.attr("data-author") + '</li>';
    stagecontent += '	<li><strong>Likes</strong>: ' + photo.attr("data-likes") + '</li>';
    stagecontent += '	<li><strong>Date</strong>: ' + obDate + '</li>';
    stagecontent += '</ul>';
    stagecontent += '<div class="instagram-nav">';
    stagecontent += '	<a class="ps-prev"></a>';
    stagecontent += '	<a class="ps-close"></a>';
    stagecontent += '	<a class="ps-next"></a>';
    stagecontent += '	<div class="clear"></div>';
    stagecontent += '</div>';
    jQuery(selector + ' .instagram-stage .instagram-content').html(stagecontent);
    if (photo.hasClass("video")) {
        var videoPoster = photo.find("img").attr("src");
        var videoURL = photo.attr("data-video");
        var videoHTML = '';
        videoHTML += '<video width="100%" height="100%" poster="' + videoPoster + '">';
        videoHTML += '    <source src="' + videoURL + '"></source>';
        videoHTML += '</video>';
        jQuery(selector + ' .instagram-stage .instagram-media').html(videoHTML);
        jQuery(selector + ' .instagram-stage .instagram-media .mejs-video').hide().fadeIn()
    } else {
        jQuery(selector + ' .instagram-stage .instagram-media').html('<img class="instagram-media-photo" src="' + photo.find("img").attr("src") + '" />');
        jQuery(selector + ' .instagram-stage .instagram-media img').hide().fadeIn()
    }
    jQuery(selector + ' .instagram-stage').slideDown();
    if( !jQuery(selector + ' .instagram-stage').hasClass('jumped') ) {
        jQuery('html,body').animate({
            scrollTop: jQuery(selector + " .instagram-stage").offset().top + 100
        })
        
    }
    jQuery(selector + ' .instagram-stage').addClass('jumped');
}

function startVideoPlayer(settings) {
    if (settings.video) {
        var ivfeatures = ['playpause', 'progress', 'volume', 'fullscreen'];
        if (settings.showVideoControls == !1) {
            ivfeatures = ['']
        }
        jQuery('.instagram-photo video, .instagram-media video').mediaelementplayer({
            alwaysShowControls: !1,
            videoVolume: 'horizontal',
            features: ivfeatures
        });
        if (settings.showVideoControls == !1) {
            jQuery(".mejs-controls").remove()
        }
    }
}

function startFancybox(settings) {
    jQuery("#" + settings.element.attr('id') + " .instagram-photo").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        helpers: {
            title: {
                type: 'inside'
            },
            overlay: {
                locked: !1
            }
        },
        beforeShow: function() {
            this.title = jQuery(this.element).attr("data-caption")
        }
    })
}

function convertDate(date, format) {
    var obDate = parseInt(date);
    obDate = new Date(obDate * 1000);
    obDate = dateFormat(obDate, format);
    return obDate
}

function escapeHtml(string) {
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };
    return String(string).replace(/[&<>"'\/]/g, function(s) {
        return entityMap[s]
    })
}

function onPhotoLoaded(data, settings, loadMoreBool) {
    console.log(data)
    if (data.pagination) {
        if (data.pagination.next_max_id) {
            settings.instagramBrowserNextMax = data.pagination.next_max_id
        } else if (data.pagination.next_max_like_id) {
            settings.instagramBrowserNextMax = data.pagination.next_max_like_id
        } else {
            settings.instagramBrowserNextMax = "Empty"
        }
    } else {
        settings.instagramBrowserNextMax = "Empty"
    }
    if (data.meta.code == 200) {
        var photos = data.data;
        if (settings.element.html() != "") {
            var addingToList = !0
        } else {
            var addingToList = !1
        }
        if (photos.length > 0) {
            for (var key in photos) {
                if (settings.limit < 200 && key >= settings.limit && settings.galleryMode != 'collage') {} else {
                    var photo = photos[key];
                console.log(photos)
                    if (settings.mode == "contest") {
                        if (photo.user_has_liked == !1) {
                            continue
                        }
                    }
                    if (settings.filter == "") {} else {
                        var tagsArray = settings.filter;
                        tagsArray = tagsArray.split(",");
                        var tagMatches = !1;
                        if (tagsArray[0]) {
                            jQuery.each(tagsArray, function(index) {
                                jQuery.each(photo.tags, function(tagindex) {
                                    if (tagsArray[index] == photo.tags[tagindex]) {
                                        tagMatches = !0
                                    }
                                })
                            })
                        } else {
                            tagMatches = !0
                        }
                        if (tagMatches == !1) {
                            continue
                        }
                    }
                    var instagramPhoto = '';
                    var photoCaption = '';
                    if (photo.caption) {
                        photoCaption = (photo.caption.text).replace(/"/g, "'")
                    } else {
                        photoCaption = "Instagram Photo"
                    }
                    if (photo.link == null) {
                        photo.link = "http://instagram.com/p/"
                    }
                    var videoURL = 'None';
                    if (photo.videos) {
                        videoURL = photo.videos.standard_resolution.url
                    }
                    if (photo.user.full_name == "" || photo.user.full_name == null) {
                        photo.user.full_name = photo.user.username
                    }
                    if (settings.enableFancybox) {
                        if (photo.type == 'video' && settings.video == !0) {
                            photo.href = '#video-' + photo.id
                        } else {
                            photo.href = photo.images.standard_resolution.url
                        }
                    } else {
                        photo.href = photo.link
                    }
                    if (photo.images.standard_resolution.height > photo.images.standard_resolution.width) {
                        photoOrientation = 'portrait-photo'
                    } else {
                        photoOrientation = 'landscape-photo'
                    }
                    if (photo.type == 'video') {
                        instagramPhoto = '<div class="insta-outer"><a class="instagram-photo video ' + photoOrientation + '" id="p' + photo.id + '" href="' + photo.href + '" target="_blank" data-id="' + photo.id + '" data-name="' + photo.user.full_name + '" data-caption-std="' + photoCaption + '" data-caption="' + photoCaption + ' &lt;a target=&quot;_blank&quot; href=&quot;' + photo.link + '&quot;&gt;View Photo&lt;/a&gt; " data-created="' + photo.created_time + '" data-author="' + photo.user.username + '" data-likes="' + photo.likes.count + '" data-comments="' + photo.comments.count + '" data-video="' + videoURL + '" data-profile="' + photo.user.profile_picture + '" rel="group" data-fancybox-group="group">';
                        instagramPhoto += '<img src="' + photo.images.standard_resolution.url + '" />';
                        // instagramPhoto += '<span class="journal-meta">' + photo.user.full_name + '<span>' + photo.user.username + '</span></span>';
                        instagramPhoto += '<span class="journal-meta">' + photo.user.full_name + '<span class="inline-name">' + photo.user.username + '</span><span class="inline-caption">'+photo.caption.text+'</span></span>';
                        instagramPhoto += '<span class="icon">Video</span>';
                        instagramPhoto += '<div id="video-' + photo.id + '" style="display:none; height:auto; width:640px;">'
                        instagramPhoto += '<video class="instagram-element-video" width="100%" height="100%">';
                        instagramPhoto += '<source src="' + videoURL + '"></source>';
                        instagramPhoto += '</video>';
                        instagramPhoto += '</div>';
                        instagramPhoto += '</a><p>'+photo.caption.text+'</p></div>'
                        // instagramPhoto += '</a></div>'
                    } else {
                        instagramPhoto = '<div class="insta-outer"><a class="instagram-photo image ' + photoOrientation + '" id="p' + photo.id + '" href="' + photo.href + '" target="_blank" data-id="' + photo.id + '" data-name="' + photo.user.full_name + '" data-caption-std="' + photoCaption + '" data-caption="' + photoCaption + ' &lt;a target=&quot;_blank&quot; href=&quot;' + photo.link + '&quot;&gt;View Photo&lt;/a&gt; " data-created="' + photo.created_time + '" data-author="' + photo.user.username + '" data-likes="' + photo.likes.count + '" data-comments="' + photo.comments.count + '" data-profile="' + photo.user.profile_picture + '" rel="group" data-fancybox-group="group">';
                        instagramPhoto += '<img src="' + photo.images.standard_resolution.url + '" />';
                        instagramPhoto += '<span class="journal-meta">' + photo.user.full_name + '<span class="inline-name">' + photo.user.username + '</span><span class="inline-caption">'+photo.caption.text+'</span></span>';
                        instagramPhoto += '<span class="icon">Image</span>';
                        instagramPhoto += '</a><p>'+photo.caption.text+'</p></div>'
                    }
                    jQuery(instagramPhoto).appendTo(settings.element)
                }
            }
            if (tagMatches == !0 || settings.filter == "") {
                var photoCount = jQuery('.insta-outer').size() - 1;
                if (addingToList == !1 && settings.isDemo == !1) {
                    jQuery('.insta-outer').hide()
                }
                jQuery('.insta-outer').each(function(index) {
                    currentPhoto = jQuery(this);
                    currentPhoto.delay(settings.delayInterval * index).fadeIn(settings.speed);
                    if (settings.limit < 20 && key >= settings.limit && settings.galleryMode != 'collage') {} else {
                        settings.element.find(".seachInstagramLoadMoreContainer").remove();
                        if (index == photoCount && settings.instagramBrowserNextMax != "Empty") {
                            jQuery('<div class="seachInstagramLoadMoreContainer"><a class="seachInstagramLoadMore btn btn-inverse">Load More</a></div>').appendTo(settings.element)
                        }
                        if (loadMoreBool == "off") {
                            jQuery(".seachInstagramLoadMoreContainer").remove()
                        }
                    }
                });
                if (settings.journalComplete == !1 || settings.galleryMode == 'contest') {
                    if (settings.mode == 'multiuser' || settings.mode == 'multitag') {
                        if (settings.multiCompleteCount >= settings.multiCompleteTotal) {
                            displayGalleryByType(settings);
                            settings.journalComplete = !0
                        }
                        settings.multiCompleteCount++
                    } else {
                        displayGalleryByType(settings);
                        settings.journalComplete = !0
                    }
                }
                startVideoPlayer(settings);
                if (settings.enableFancybox) {
                    startFancybox(settings)
                }
            }
        } else {
            alert('empty')
        }
    } else {
        alert(data.meta.error_message)
        console.error(data.meta.error_message)
    }
}

function displayGalleryByType(settings) {
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i)
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i)
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i)
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i)
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            8
        }
    };
    if (settings.galleryMode == 'classic') {
        jQuery(document).on({
            mouseenter: function() {
                var size = jQuery(this).find('.journal-meta').outerHeight();
                jQuery(this).find('.journal-meta').stop().animate({
                    'opacity': 1,
                    'top': '50%',
                    'margin-top': '-' + size / 2 + 'px'
                }, 300);
                if (isMobile.any()) {
                    jQuery(this).trigger("click")
                }
            },
            mouseleave: function() {
                jQuery(this).find('.journal-meta').stop().animate({
                    'opacity': 0,
                    'top': '90%'
                }, 300, function() {
                    jQuery(this).removeAttr('style')
                })
            }
        }, settings.element.selector + " .instagram-photo");
        jQuery(document).on("click", settings.element.selector + " .instagram-photo", function() {
            if (settings.enableFancybox) {} else {
                jQuery(settings.element.selector + " .instagram-photo").removeClass("active");
                jQuery(this).addClass("active");
                explorePhoto(jQuery(this), settings.element.selector);
                startVideoPlayer(settings);
                if (!settings.enableLinks) {
                    jQuery(settings.element.selector + ' .instagram-stage p a').remove()
                }
                return !1
            }
        });
        jQuery(document).on("click", settings.element.selector + " .instagram-nav a.ps-close", function() {
            jQuery(settings.element.selector + ' .instagram-stage').slideUp().removeClass('active')
        });
        jQuery(document).on("click", settings.element.selector + " .instagram-nav a.ps-next", function() {
            var currentPhoto = jQuery(settings.element.selector + " .instagram-photo.active");
            var nextPhoto = jQuery(settings.element.selector + " .instagram-photo.active").parents('.insta-outer').next(settings.element.selector + " .insta-outer").find(".instagram-photo");
            if (nextPhoto.index() == -1) {
                nextPhoto = jQuery(settings.element.selector + " .instagram-photo").first()
            }
            jQuery(settings.element.selector + " .instagram-media .instagram-media-photo, " + settings.element.selector + " .instagram-media video").fadeOut(function() {
                explorePhoto(nextPhoto, settings.element.selector);
                startVideoPlayer(settings);
                if (!settings.enableLinks) {
                    jQuery(settings.element.selector + ' .instagram-stage p a').remove()
                }
            });
            jQuery(settings.element.selector + " .instagram-photo").removeClass("active");
            jQuery(nextPhoto).addClass("active")
        });
        jQuery(document).on("click", settings.element.selector + " .instagram-nav a.ps-prev", function() {
            var currentPhoto = jQuery(settings.element.selector + " .instagram-photo.active");
            var prevPhoto = jQuery(settings.element.selector + " .instagram-photo.active").parents('.insta-outer').prev(settings.element.selector + " .insta-outer").find(".instagram-photo");
            if (prevPhoto.index() == -1) {
                prevPhoto = jQuery(settings.element.selector + " .instagram-photo").last()
            }
            jQuery(settings.element.selector + " .instagram-media .instagram-media-photo, " + settings.element.selector + " .instagram-media video").fadeOut(function() {
                explorePhoto(prevPhoto, settings.element.selector);
                startVideoPlayer(settings);
                if (!settings.enableLinks) {
                    jQuery(settings.element.selector + ' .instagram-stage p a').remove()
                }
            });
            jQuery(settings.element.selector + " .instagram-photo").removeClass("active");
            jQuery(prevPhoto).addClass("active")
        })
    }
    if (settings.galleryMode == 'collage') {
        jQuery(settings.element.selector + " .instagram-photo").each(function(index) {
            index = index + 1;
            if (index < 7) {
                if (jQuery(this).hasClass("disabled")) {
                    jQuery(this).remove();
                    return !0
                }
                if (index % 3 == 0) {
                    instagramPhoto = '<li>';
                    instagramPhoto += '<div class="instagram-photo one" data-fancybox-href="' + jQuery(this).attr("href") + '" data-name="' + jQuery(this).attr("data-name") + '" id=p"' + jQuery(this).attr("id") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).attr("data-caption")) + '" data-fancybox-group="group">';
                    if (jQuery(this).hasClass("video") && settings.video == !0) {
                        if (settings.enableFancybox) {
                            instagramPhoto += '<div id="video-' + jQuery(this).attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                            instagramPhoto += '<video width="100%" height="100%">';
                            instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                            instagramPhoto += '</video>';
                            instagramPhoto += '</div>';
                            instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                            instagramPhoto += '	<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                        } else {
                            instagramPhoto += '<video width="100%" height="315" poster="' + jQuery(this).find("img").attr("src") + '">';
                            instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                            instagramPhoto += '</video>'
                        }
                    } else {
                        instagramPhoto += '	<img src="' + jQuery(this).find("img").attr("src") + '">';
                        instagramPhoto += '	<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                    }
                    instagramPhoto += '</div>';
                    instagramPhoto += '</li>'
                } else {
                    instagramPhoto = '<li>';
                    instagramPhoto += '<div class="instagram-photo two" data-fancybox-href="' + jQuery(this).attr("href") + '" data-name="' + jQuery(this).attr("data-name") + '" id=p"' + jQuery(this).attr("id") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).attr("data-caption")) + '" data-fancybox-group="group">';
                    if (jQuery(this).hasClass("video") && settings.video == !0) {
                        if (settings.enableFancybox) {
                            instagramPhoto += '<div id="video-' + jQuery(this).attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                            instagramPhoto += '<video width="100%" height="100%">';
                            instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                            instagramPhoto += '</video>';
                            instagramPhoto += '</div>';
                            instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                            instagramPhoto += '	<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                        } else {
                            instagramPhoto += '<video width="100%" height="315" poster="' + jQuery(this).find("img").attr("src") + '">';
                            instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                            instagramPhoto += '</video>'
                        }
                    } else {
                        instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                        instagramPhoto += '<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                    }
                    instagramPhoto += '</div>';
                    instagramPhoto += '<div class="instagram-photo two" data-fancybox-href="' + jQuery(this).next().attr("href") + '" data-name="' + jQuery(this).next().attr("data-name") + '" id=p"' + jQuery(this).next().attr("id") + '" data-created="' + jQuery(this).next().attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).next().attr("data-caption")) + '" data-fancybox-group="group">';
                    if (jQuery(this).next().hasClass("video") && settings.video == !0) {
                        if (settings.enableFancybox) {
                            instagramPhoto += '<div id="video-' + jQuery(this).next().attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                            instagramPhoto += '<video width="100%" height="100%">';
                            instagramPhoto += '<source src="' + jQuery(this).next().attr("data-video") + '"></source>';
                            instagramPhoto += '</video>';
                            instagramPhoto += '</div>';
                            instagramPhoto += '<img src="' + jQuery(this).next().find("img").attr("src") + '">';
                            instagramPhoto += '<a class="overlay" target="_blank" href="' + jQuery(this).next().attr("href") + '">' + jQuery(this).next().attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).next().attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                        } else {
                            instagramPhoto += '<video width="100%" height="315" poster="' + jQuery(this).next().find("img").attr("src") + '">';
                            instagramPhoto += '<source src="' + jQuery(this).next().attr("data-video") + '"></source>';
                            instagramPhoto += '</video>'
                        }
                    } else {
                        instagramPhoto += '<img src="' + jQuery(this).next().find("img").attr("src") + '">';
                        instagramPhoto += '<a class="overlay" target="_blank" href="' + jQuery(this).next().attr("href") + '">' + jQuery(this).next().attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).next().attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                    }
                    instagramPhoto += '</div>';
                    instagramPhoto += '</li>';
                    jQuery(this).next().addClass("disabled")
                }
                jQuery(this).remove();
                jQuery(instagramPhoto).appendTo(settings.element.find(".jcarousel.large ul"))
            }
            if (index >= 7 && index < 16) {
                instagramPhoto = '<li>'
                instagramPhoto += '<div class="instagram-photo one" data-fancybox-href="' + jQuery(this).attr("href") + '" data-name="' + jQuery(this).attr("data-name") + '" id="p' + jQuery(this).attr("id") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).attr("data-caption")) + '" data-fancybox-group="group">';
                if (jQuery(this).hasClass("video") && settings.video == !0) {
                    if (settings.enableFancybox) {
                        instagramPhoto += '<div id="video-' + jQuery(this).attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                        instagramPhoto += '<video width="100%" height="100%" poster="' + jQuery(this).find("img").attr("src") + '">';
                        instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                        instagramPhoto += '</video>';
                        instagramPhoto += '</div>';
                        instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                        instagramPhoto += '<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                    } else {
                        instagramPhoto += '<video width="100%" height="320" poster="' + jQuery(this).find("img").attr("src") + '">';
                        instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                        instagramPhoto += '</video>'
                    }
                } else {
                    instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                    instagramPhoto += '<a class="overlay" target="_blank" href="' + jQuery(this).attr("href") + '">' + jQuery(this).attr("data-name") + ' <span class="subtitle">' + convertDate(jQuery(this).attr("data-created"), "mmmm yyyy") + '</span><em class="btn"><span>View Photo</span></em></a>'
                }
                instagramPhoto += '</div>';
                instagramPhoto += '</li>';
                jQuery(this).remove();
                jQuery(instagramPhoto).appendTo(settings.element.find(".jcarousel.med ul"))
            }
        });
        if (jQuery(settings.element.selector + ' .jcarousel').size() > 0) {
            jQuery(settings.element.selector + ' .jcarousel.large').jcarousel({
                wrap: 'circular',
                animation: {
                    duration: settings.carouselDuration,
                    easing: settings.carouselEasing
                }
            }).jcarouselAutoscroll({
                interval: 3000,
                target: '+=2',
                autostart: settings.carouselAutostart
            }).on('jcarousel:scroll jcarousel:reload', function() {
                var element = jQuery(this),
                    width = element.innerWidth();
                if (width < 1255) {
                    element.jcarousel('items').each(function() {
                        if (jQuery(this).find(".instagram-photo.two").size() > 0) {
                            jQuery(this).css('width', width + 'px')
                        } else {
                            jQuery(this).css('width', width / 2 + 'px')
                        }
                    });
                    jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                        target: '-=1'
                    });
                    jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                        target: '+=1'
                    })
                } else {
                    element.jcarousel('items').removeAttr('style');
                    jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                        target: '-=' + settings.carouselScroll
                    });
                    jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                        target: '+=' + settings.carouselScroll
                    })
                }
            });
            jQuery(settings.element.selector + ' .jcarousel.med').jcarousel({
                wrap: 'circular',
                animation: {
                    duration: settings.carouselDuration,
                    easing: settings.carouselEasing
                }
            }).jcarouselAutoscroll({
                interval: 4000,
                target: '+=2',
                autostart: settings.carouselAutostart
            }).on('jcarousel:scroll jcarousel:reload', function() {
                var element = jQuery(this),
                    width = element.innerWidth();
                if (width < 1255) {
                    width = width / 2;
                    element.jcarousel('items').css('width', width + 'px')
                } else {
                    element.jcarousel('items').removeAttr('style')
                }
            });
            jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                target: '-=' + settings.carouselScroll
            });
            jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                target: '+=' + settings.carouselScroll
            });
            jQuery(settings.element.selector + ' .jcarousel').jcarousel('scroll', 0)
        }
        jQuery(document).on({
            mouseenter: function() {
                jQuery(this).find(".overlay").stop().fadeIn("fast")
            },
            mouseleave: function() {
                jQuery(this).find(".overlay").stop().fadeOut("fast")
            }
        }, ".collage-view .instagram-photo");
        if (!settings.enableLinks) {
            jQuery(settings.element.selector + " .overlay").html("")
        }
        jQuery(document).on("click", settings.element.selector + " .instagram-photo", function() {
            if (!settings.enableLinks) {
                return !1
            }
        })
    }
    if (settings.galleryMode == 'section') {
        jQuery(settings.element.selector + " .instagram-photo").each(function(index) {
            instagramPhoto = '<li>';
            instagramPhoto += '	<a href="' + jQuery(this).attr("href") + '" target="_blank" class="' + jQuery(this).attr("class") + '" data-name="' + jQuery(this).attr("data-name") + '" id="p' + jQuery(this).attr("id") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).attr("data-caption")) + '" rel="group">';
            if (jQuery(this).hasClass("video") && settings.video == !0) {
                if (settings.enableFancybox) {
                    instagramPhoto += '<div id="video-' + jQuery(this).attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                    instagramPhoto += '<video width="100%" height="100%">';
                    instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                    instagramPhoto += '</video>';
                    instagramPhoto += '</div>';
                    instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">'
                } else {
                    instagramPhoto += '<video width="100%" height="100%" poster="' + jQuery(this).find("img").attr("src") + '">';
                    instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                    instagramPhoto += '</video>'
                }
            } else {
                instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                if (settings.enableLinks) {
                    instagramPhoto += '<span class="view-photo">View Photo</span>'
                }
            }
            instagramPhoto += '	</a>';
            instagramPhoto += '	<div class="post-text">';
            instagramPhoto += '		<h4>' + jQuery(this).attr("data-name") + '</h4>';
            instagramPhoto += '		<p>' + jQuery(this).attr("data-caption") + '</p>';
            instagramPhoto += '	</div>';
            instagramPhoto += '	<div class="post-meta">';
            instagramPhoto += '		<h5>' + convertDate(jQuery(this).attr("data-created"), "mmmm dS, yyyy") + '</h5>';
            instagramPhoto += '		<span class="meta">' + jQuery(this).attr("data-likes") + ' Likes - ' + jQuery(this).attr("data-comments") + ' Comments</span>';
            instagramPhoto += '	</div>';
            instagramPhoto += '</li>';
            jQuery(this).remove();
            jQuery(instagramPhoto).appendTo(settings.element.find(".jcarousel ul"))
        });
        settings.element.find(".seachInstagramLoadMore").remove();
        jQuery(settings.element.selector + ' .jcarousel').jcarousel({
            wrap: 'circular',
            animation: {
                duration: settings.carouselDuration,
                easing: settings.carouselEasing
            }
        }).on('jcarousel:scroll jcarousel:reload', function() {
            var element = jQuery(this),
                width = element.innerWidth();
            if (width < 1060 && width > 780) {
                width = (width / 3) - 10;
                element.jcarousel('items').css('width', width + 'px');
                jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                    target: '-=3'
                });
                jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                    target: '+=3'
                })
            } else if (width < 780) {
                width = (width / 2) - 10;
                element.jcarousel('items').css('width', width + 'px');
                jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                    target: '-=2'
                });
                jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                    target: '+=2'
                })
            } else {
                element.jcarousel('items').removeAttr('style');
                jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
                    target: '-=' + settings.carouselScroll
                });
                jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
                    target: '+=' + settings.carouselScroll
                })
            }
        });
        jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
            target: '-=' + settings.carouselScroll
        });
        jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
            target: '+=' + settings.carouselScroll
        });
        jQuery(settings.element.selector + ' .jcarousel').jcarousel('scroll', 0);
        jQuery(document).on("click", settings.element.selector + " .instagram-photo", function() {
            if (settings.video == !0 || settings.enableLinks == !1) {
                if (jQuery(this).hasClass("video")) {
                    return !1
                }
            }
        })
    }
    if (settings.galleryMode == 'large') {
        jQuery(settings.element.selector + " .instagram-photo").each(function(index) {
            instagramPhoto = '<li>';
            if ((jQuery(this).attr("data-caption-std")).length >= 560) {
                var photoCaption = (jQuery(this).attr("data-caption-std")).substring(0, 560) + "..."
            } else {
                var photoCaption = jQuery(this).attr("data-caption-std")
            }
            if (jQuery(this).hasClass("video") && settings.video == !0) {
                instagramPhoto += '<a class="' + jQuery(this).attr("class") + '" data-url="' + jQuery(this).attr("href") + '" data-name="' + jQuery(this).attr("data-name") + '" data-caption-std="' + photoCaption + '" data-author="' + jQuery(this).attr("data-name") + '" data-likes="' + jQuery(this).attr("data-likes") + '" data-created="' + jQuery(this).attr("data-created") + '">';
                instagramPhoto += '<video width="100%" height="100%" poster="' + jQuery(this).find("img").attr("src") + '">';
                instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                instagramPhoto += '</video>';
                instagramPhoto += '</a>'
            } else {
                instagramPhoto += '<a class="' + jQuery(this).attr("class") + '" data-url="' + jQuery(this).attr("href") + '" data-name="' + jQuery(this).attr("data-name") + '" data-caption-std="' + photoCaption + '" data-author="' + jQuery(this).attr("data-name") + '" data-likes="' + jQuery(this).attr("data-likes") + '" data-created="' + jQuery(this).attr("data-created") + '">';
                instagramPhoto += '<img src="' + jQuery(this).find("img").attr("src") + '">';
                instagramPhoto += '</a>'
            }
            instagramPhoto += '</li>';
            jQuery(this).remove();
            jQuery(instagramPhoto).appendTo(settings.element.find(".jcarousel ul"))
        });
        settings.element.find(".seachInstagramLoadMore").remove();
        jQuery(settings.element.selector + ' .jcarousel').jcarousel({
            wrap: 'circular',
            animation: {
                duration: settings.carouselDuration,
                easing: settings.carouselEasing,
                transitions: !0
            }
        }).on('jcarousel:scrollend', function(event, carousel, target, animate) {
            var obDate = parseInt(jQuery(this).jcarousel('first').find('.instagram-photo').attr("data-created"));
            obDate = new Date(obDate * 1000);
            obDate = dateFormat(obDate, "dddd, mmmm dS, yyyy");
            var currentPhoto = jQuery(this).jcarousel('first').find('.instagram-photo');
            var navHtml = "";
            if (currentPhoto.hasClass("video") && settings.video == !0) {
                navHtml += '<img src="' + currentPhoto.find('video').attr('poster') + '">'
            } else {
                navHtml += '<img src="' + currentPhoto.find('img').attr('src') + '">'
            }
            navHtml += '<h2>' + currentPhoto.attr('data-name') + '</h2>';
            navHtml += '<ul>';
            navHtml += '	<li>by <strong>' + currentPhoto.attr('data-author') + '</strong></li>';
            navHtml += '	<li>' + obDate + '</li>';
            navHtml += '	<li>' + currentPhoto.attr('data-likes') + ' <strong>Likes</strong></li>';
            navHtml += '	<li>' + currentPhoto.attr('data-likes') + ' <strong>Comments</strong></li>';
            navHtml += '</ul>';
            navHtml += '<p>' + currentPhoto.attr('data-caption-std') + '</p>';
            if (settings.enableLinks) {
                navHtml += '<a href="' + currentPhoto.attr('data-url') + '" target="_blank">Continue reading...</a>'
            }
            jQuery(settings.element.selector + " .navigator").html(navHtml)
        }).on('jcarousel:scroll jcarousel:reload', function() {
            var element = jQuery(this),
                width = element.innerWidth();
            if (width < 886) {
                width = width / 2;
                element.jcarousel('items').css('width', width + 'px')
            } else {
                element.jcarousel('items').removeAttr('style')
            }
        });
        jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
            target: '-=' + settings.carouselScroll
        });
        jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
            target: '+=' + settings.carouselScroll
        });
        jQuery(settings.element.selector + ' .jcarousel').jcarousel('scroll', 0);
        jQuery(document).on("click", settings.element.selector + " .instagram-photo", function() {
            if (settings.video == !1) {
                var target = jQuery(settings.element.selector + " .instagram-photo").index(jQuery(this));
                jQuery(settings.element.selector + ' .jcarousel').jcarousel('scroll', target)
            }
        })
    }
    if (settings.galleryMode == 'infinity') {
        jQuery(settings.element.selector + " .instagram-photo").each(function(index) {
            instagramPhoto = '<li>';
            if (settings.enableLinks) {
                var linkHTML = 'href="' + jQuery(this).attr("href") + '"'
            } else {
                var linkHTML = ''
            }
            instagramPhoto += '<a class="' + jQuery(this).attr("class") + '" ' + linkHTML + ' target="_blank" data-author="' + jQuery(this).attr("data-name") + '" data-likes="' + jQuery(this).attr("data-likes") + '" data-profile="' + jQuery(this).attr("data-profile") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + escapeHtml(jQuery(this).attr("data-caption")) + '" rel="group">';
            if (jQuery(this).hasClass("video") && settings.video == !0) {
                if (settings.enableFancybox) {
                    instagramPhoto += '<div id="video-' + jQuery(this).attr("data-id") + '" style="display:none; height:auto; width:640px;">';
                    instagramPhoto += '<video width="100%" height="100%">';
                    instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                    instagramPhoto += '</video>';
                    instagramPhoto += '</div>';
                    instagramPhoto += '<img class="photo" src="' + jQuery(this).find("img").attr("src") + '">'
                } else {
                    instagramPhoto += '<video width="195" height="195" poster="' + jQuery(this).find("img").attr("src") + '">';
                    instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                    instagramPhoto += '</video>'
                }
            } else {
                instagramPhoto += '<img class="photo" src="' + jQuery(this).find("img").attr("src") + '">'
            }
            instagramPhoto += '</a>';
            instagramPhoto += '</li>';
            jQuery(this).remove();
            jQuery(instagramPhoto).appendTo(settings.element.find(".jcarousel ul"))
        });
        if (settings.limit < 10) {
            var infinityPhotos = jQuery((settings.element).selector + " .instagram-photo").parent().parent().html();
            for (var d = 1; d < (15 / settings.limit); d++) {
                settings.element.find(".jcarousel ul").append(infinityPhotos)
            }
        }
        settings.element.find(".seachInstagramLoadMore").remove();
        jQuery(settings.element.selector + ' .jcarousel').jcarousel({
            wrap: 'circular',
            animation: {
                duration: settings.carouselDuration,
                easing: settings.carouselEasing,
                transitions: !0
            }
        }).jcarouselAutoscroll({
            interval: 0,
            target: '+=1',
            autostart: settings.carouselAutostart
        });
        jQuery(settings.element.selector + ' .jcarousel-control-prev').jcarouselControl({
            target: '-=' + settings.carouselScroll
        });
        jQuery(settings.element.selector + ' .jcarousel-control-next').jcarouselControl({
            target: '+=' + settings.carouselScroll
        });
        jQuery(document).on({
            mouseenter: function() {
                if (settings.video == !1 || settings.enableFancybox) {
                    if ((jQuery(this).attr("data-caption")).length > 120) {
                        var ellipsis = '...'
                    } else {
                        var ellipsis = ''
                    }
                    var profile = '';
                    profile += '<div class="profile">';
                    profile += '	<img src="' + jQuery(this).attr("data-profile") + '" />';
                    profile += '	<h3>' + jQuery(this).attr("data-author") + '</h3>';
                    profile += '	<em>' + convertDate(jQuery(this).attr("data-created"), "mmmm dd, yyyy") + ' <span>' + jQuery(this).attr("data-likes") + ' Likes</span></em>';
                    profile += '	<p>' + (jQuery(this).attr("data-caption")).substring(0, 120) + ellipsis + '</p>';
                    profile += '</div>';
                    jQuery(this).append(profile);
                    jQuery(this).find(".profile").hide().fadeIn()
                }
            },
            mouseleave: function() {
                if (settings.video == !1 || settings.enableFancybox) {
                    jQuery(this).find(".profile").remove()
                }
            }
        }, settings.element.selector + " .instagram-photo");
        jQuery(document).on("click", settings.element.selector + " .instagram-photo", function() {
            if (settings.video == !0) {
                return !1
            }
        })
    }
    if (settings.galleryMode == 'contest') {
        jQuery(settings.element.selector + " .instagram-photo").not(".contest-photo").each(function(index) {
            instagramPhoto = '';
            instagramPhoto += '<div class="insta-outer"><a class="instagram-photo contest-photo" href="' + jQuery(this).attr("href") + '" target="_blank" data-author="' + jQuery(this).attr("data-name") + '" data-likes="' + jQuery(this).attr("data-likes") + '" data-profile="' + jQuery(this).attr("data-profile") + '" data-created="' + jQuery(this).attr("data-created") + '" data-caption="' + jQuery(this).attr("data-caption-std") + '">';
            instagramPhoto += '<div class="instagram-media">';
            if (jQuery(this).hasClass("video") && settings.video == !0) {
                instagramPhoto += '<video width="195" height="195" poster="' + jQuery(this).find("img").attr("src") + '">';
                instagramPhoto += '<source src="' + jQuery(this).attr("data-video") + '"></source>';
                instagramPhoto += '</video>'
            } else {
                instagramPhoto += '<img class="photo" src="' + jQuery(this).find("img").attr("src") + '">'
            }
            instagramPhoto += '</div>';
            instagramPhoto += '<div class="instagram-content">';
            instagramPhoto += '<span class="author">' + jQuery(this).attr("data-author") + '</span>';
            instagramPhoto += '<span class="date">' + convertDate(jQuery(this).attr("data-created"), "dddd, mmmm dS, yyyy, h:MM TT") + '</span>';
            instagramPhoto += '<span class="likes">' + jQuery(this).attr("data-likes") + '</span>';
            instagramPhoto += '</div>';
            instagramPhoto += '</a></div>';
            jQuery(this).remove();
            jQuery(instagramPhoto).appendTo(settings.element.find('.contest-collection'))
        })
    }
}
jQuery.fn.instagramJournal = function(options) {
    var settings = {
        accessToken: '',
        mode: 'user',
        userID: '1138644',
        tag: 'sports',
        locationID: '230548',
        galleryMode: 'classic',
        video: !1,
        speed: 700,
        delayInterval: 80,
        showVideoControls: !1,
        carouselAutostart: !0,
        carouselScroll: 1,
        carouselEasing: 'linear',
        carouselDuration: 500,
        count: 20,
        limit: 20,
        enableLinks: !0,
        enableFancybox: !0,
        filter: ""
    };
    settings.element = jQuery(this);
    settings.instagramBrowserNextMax = "";
    settings.journalComplete = !1;
    settings.isDemo = !1;
    jQuery.extend(settings, options);
    return this.each(function() {
        jQuery(document).ready(function() {
            instagramFetch(settings);
            jQuery(document).on("click", settings.element.selector + " .seachInstagramLoadMore", function() {
                if (settings.mode == 'tag' || settings.mode == 'contest') {
                    instagramTagsLoadMore(settings)
                } else if (settings.mode == 'user') {
                    instagramUsersLoadMore(settings)
                } else if (settings.mode == 'location') {
                    instagramLocationLoadMore(settings)
                } else if (settings.mode == 'liked') {
                    instagramLikedLoadMore(settings)
                }
            });
            jQuery("#searchByTag .submit").click(function() {
                if (settings.galleryMode == "classic") {
                    settings.element.find(".instagram-photo").remove();
                    settings.element.find(".instagram-user-all, .ig-glyph").remove();
                    var customSettings = settings;
                    customSettings.mode = 'tag';
                    customSettings.tag = jQuery("input.searchTag").val().replace(/ /g, '');
                    customSettings.isDemo = !0;
                    customSettings.video = !0;
                    customSettings.showVideoControls = !0;
                    if (customSettings.tag == "") {
                        alert("Please enter a valid hashtag.");
                        return !1
                    }
                    instagramFetch(customSettings);
                    jQuery('html,body').animate({
                        scrollTop: settings.element.offset().top - 70
                    });
                    return !1
                }
            });
            jQuery("#searchByUser .submit").click(function() {
                if (settings.galleryMode == "classic") {
                    settings.element.find(".instagram-photo").remove();
                    settings.element.find(".instagram-user-all, .ig-glyph").remove();
                    settings.element.find(".seachInstagramLoadMoreContainer").remove();
                    var customSettings = settings;
                    customSettings.mode = 'user';
                    customSettings.user = jQuery("input.searchUser").val().replace(/ /g, '');
                    customSettings.isDemo = !0;
                    customSettings.video = !0;
                    customSettings.showVideoControls = !0;
                    if (customSettings.user == "") {
                        alert("Please enter a valid username.");
                        return !1
                    }
                    instagramUserSearch(customSettings);
                    jQuery('html,body').animate({
                        scrollTop: settings.element.offset().top - 150
                    });
                    return !1
                }
            });
            jQuery(document).on("click", ".instagram-user-all", function() {
                if (settings.galleryMode == "classic") {
                    settings.element.find(".instagram-photo").remove();
                    settings.element.find(".instagram-user-all, .ig-glyph").remove();
                    var customSettings = settings;
                    customSettings.mode = 'user';
                    customSettings.userID = jQuery(this).attr("rel");
                    customSettings.isDemo = !0;
                    instagramFetch(customSettings)
                }
            });
            jQuery("#searchByLocation .submit").click(function() {
                if (settings.galleryMode == "classic") {
                    settings.element.find(".instagram-photo").remove();
                    settings.element.find(".instagram-user-all, .ig-glyph").remove();
                    settings.element.find(".seachInstagramLoadMoreContainer").remove();
                    var customSettings = settings;
                    customSettings.mode = 'location';
                    customSettings.locationID = jQuery("select.searchLocation").val().replace(/ /g, '');
                    customSettings.isDemo = !0;
                    customSettings.video = !0;
                    customSettings.showVideoControls = !0;
                    if (customSettings.locationID == "") {
                        alert("Please enter a valid location.");
                        return !1
                    }
                    instagramFetch(customSettings);
                    jQuery('html,body').animate({
                        scrollTop: settings.element.offset().top - 70
                    });
                    return !1
                }
            })
        })
    })
}