/*
 * Instagram Journal Demo
 * Version: 1.0
 *
 * Author: Chris Rivers
 * http://chrisriversdesign.com
 *
 *
 * Changelog: 
 * Version: 1.0
 *
 */


$(document).ready(function(){
	
	// Dynamic Backgrounds
	$(".slide").not(".no-bg").each(function(){
		// Check If Photo Exists.
		if( $(this).find(".slide-photo img").length != 0 ){
			$(this).css("background-image", "url("+ $(this).find(".slide-photo img").attr("src") +")");
			$(this).find(".slide-photo").remove();
		}
	});
	
	// Scroll Events
	$(window).scroll(function(){
		scrollSpy();
	});
	
	$(".get-started").click(function(){
		// Scroll To Stage
		$('html,body').animate({
		   scrollTop: $(".classic-layout").offset().top - 68
		});
	});
	
	// Navigation Scroll Events		
	$("#header .navigation a, #header .logo a, a.scrollTo").click(function(){
		gotoScrollSpy( ($(this).attr("href")), 800 );
		return false;
	});
	
	// Mobile Navigation
	$("#header .btn-navbar").click(function(){
		$("#header ul.navigation").toggleClass("collapse");
	});
	
	$("#header ul.navigation li a").click(function(){
		$("#header ul.navigation").toggleClass("collapse");
	});
	
	// Customization Bar
	$(".ig-controller-toggle").click(function(){
		
		$(this).toggleClass("active");
		
		if( $(this).hasClass("active") ){
			// OPEN
			$(".ig-controller").stop().animate({
				right: ["0", "easeOutQuad"]
			}, 600);
			
		} else {
			// CLOSE
			$(".ig-controller").stop().animate({
				right: ["-250", "easeOutQuad"]
			}, 600);
		}
		
	});
	
	// Customization Bar - Theme Accent Color
	$(".ig-controller_container a").click(function(){
		// Remove Old
		$("body").removeClass("ig-business");
		$("body").removeClass("ig-dream");
		$("body").removeClass("ig-success");
		$("body").removeClass("ig-pinterest");
		$("body").removeClass("ig-ego");
		$("body").removeClass("ig-fashion");
		$(".ig-controller_container a").removeClass("active");
		
		// Add New
		$("body").addClass($(this).attr("class"));
		$(this).addClass("active");
	});
	
	// Delay
	$(".ig-controller").addClass("hidden");
	
	// Select User Glyph
	var interval = setInterval(animateGlyph, 700);
	function animateGlyph() { $(".ig-glyph").toggleClass('on'); }
	
	// Detect When Element is visible, good for specific elements.
	function isScrolledIntoView(elem){	
		if( $(elem).size() > 0 ){
			// New & Improved Check
			var docViewTop = $(window).scrollTop(),
			docViewBottom = docViewTop + $(window).height(),
			elemTop = $(elem).offset().top,
			elemBottom = elemTop + $(elem).height();
	
			// Is more than half of the element visible
			return ((elemTop + ((elemBottom - elemTop)/2)) >= docViewTop && ((elemTop + ((elemBottom - elemTop)/2)) <= docViewBottom));
		}
	}
	
	// Scrollspy
	function gotoScrollSpy(id, speed){
		var curSlide = $(".slide[rel='"+ (id).replace("#","") +"']");
	
		if( curSlide.size() > 0 ){	
			$('html,body').animate({scrollTop: (curSlide).offset().top - 66 }, speed);
		}
	}
	
	function scrollSpy() {
		var a,b,c;
		$("ul.navigation li a").each(function(){
			curSlide = $(".slide[rel='"+ ($(this).attr("href")).replace("#","") +"']");
		
			if( curSlide.size() > 0 ){	
				a = curSlide.offset().top - 250;
				b = curSlide.offset().top + curSlide.next(".slide").outerHeight();
				c = $(window).scrollTop();

				if (c >= a && c <= b) {
					$("ul.navigation li a").removeClass("active");
					$(this).addClass("active");
					
					$(".ig-controller").removeClass("hidden");
				}
			}
		});
		
		if( $(window).scrollTop() < 1 ){
			$("ul.navigation li a").removeClass("active");
		}
	}
	
});