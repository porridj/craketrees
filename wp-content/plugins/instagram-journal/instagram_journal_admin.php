<?php
// Save
if($_POST){
	if($_POST['isjournal_hidden'] == 'Y') {
		// Post View
		$isjournalaccesstoken = $_POST['isjournal_accesstoken'];
		update_option('isjournal_accesstoken', $isjournalaccesstoken);
		
		$isjournalcontesttag = $_POST['isjournal_contest_tag'];
		update_option('isjournal_contest_tag', $isjournalcontesttag);
			
		$isjournalcontestmode = $_POST['isjournal_contest_mode'];
		update_option('isjournal_contest_mode', $isjournalcontestmode);
		
		$isjournalcontestgallerymode = $_POST['isjournal_contest_gallery_mode'];
		update_option('isjournal_contest_gallery_mode', $isjournalcontestgallerymode);
			
		?>
		<div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
		<?php
	
	}
} else {
	// Normal View
	$isjournalaccesstoken = get_option('isjournal_accesstoken');
	$isjournalcontesttag = get_option('isjournal_contest_tag');
	$isjournalcontestmode = get_option('isjournal_contest_mode');
	$isjournalcontestgallerymode = get_option('isjournal_contest_gallery_mode');
}

?>

<div class="updated contest-updated" style="display:none;"><p><strong><?php _e('New photos saved.' ); ?></strong></p></div>

<div class="wrap rm_wrap">
	<h2 id="nhp-opts-heading">Instagram Journal Settings</h2>
		
	<img style="margin-bottom: 7px; width:100%;" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/instagram-journal/code/css/images/admin-view.png">
 
	<div class="rm_opts">
		<form method="post" id="nhp-opts-form-wrapper">
			<input type="hidden" name="isjournal_hidden" value="Y">
						
			<div class="rm_section">
				<div class="rm_title">
					<h3 class="active">
						<img alt="" class="active" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/instagram-journal/code/functions/images/trans.png">
						Basic Settings
					</h3>
					<span class="submit">
						<input class="button button-primary" type="submit" value="Save changes" name="save1">
					</span>
					<div class="clearfix"></div>
				</div>
				
				<div class="rm_options" style="display: block;">
					<div class="rm_input rm_textarea">
                        <label>Access Token:</label>
                        <input class="username" type="text" name="isjournal_accesstoken" value="<?php echo $isjournalaccesstoken; ?>" />
                        <small>Enter your Instagram access token. This is used by the plugin to pull in your photos.</small>
						<div class="clearfix"></div>
                    </div>
				</div>
			</div>
						
			<div class="rm_section">
				<div class="rm_title">
					<h3 class="active">
						<img alt="" class="active" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/instagram-journal/code/functions/images/trans.png">
						Contest Mode Settings
					</h3>
					<span class="submit">
						<input class="button button-primary" type="submit" value="Save changes" name="save1">
					</span>
					<div class="clearfix"></div>
				</div>
				
				<div class="rm_options" style="display: block;">
					
					<div class="rm_input rm_textarea">
                        <label>Contest Tag:</label>
                        <input class="tag" type="text" name="isjournal_contest_tag" value="<?php echo $isjournalcontesttag; ?>" />
                        <small>Enter a tag for the contest. <br />Eg: sports, girls, travel ect.</small>
						<div class="clearfix"></div>
                    </div>
					
					<div class="rm_input rm_textarea rm_instagram_moderation">
						<label>Moderation Mode:</label>
					
						<div class="radio-container">
							<input type="radio" name="isjournal_contest_mode"  value="mobile" <?php if( $isjournalcontestmode != 'wordpress' ){ echo "checked='checked'"; }?> />
							Mobile Moderation<br />
						
							<input type="radio" name="isjournal_contest_mode" value="wordpress" <?php if( $isjournalcontestmode == 'wordpress' ){ echo "checked='checked'"; }?> />
							Wordpress Moderation<br />
						</div>
					
						<small>
							This feature allows you to choose how you would like to moderate photos.
						</small>
						<div class="clearfix"></div>	
					</div>
					
					<div class="rm_input wordpress-moderation">
						<label>Manage Photos</label>
						
						<div class="new-photos">
							
							<label>Available Photos (Instagram API)</label><br /> 
							Select the photos you would like to approve and click the save button.<br /><br />
							
							<div class="instagram-journal-choose"></div>
							
							<a class="button button-primary add-photos-btn">Save Selected Photos</a>
							
							<div class="clear"></div>
						</div>
						
						<div class="current-photos">							
							<label>Approved Photos</label><br /> 
							See one you don't like, remove it now!<br /><br />
							
							<?php 	
								// Get Options
								$contestEntries = get_option('isjournal_contest_entries');
								$journalAdminURL = admin_url().'admin.php?page=instagramJournal';
														
								// Build
								if( $contestEntries ){										
									echo '<ul>';
										foreach( $contestEntries as $contestEntry ){
											echo '<li>';
											echo	'<img src="'.$contestEntry['image'].'">';
											echo	'<a href="'.$journalAdminURL.'&deleteContestEntry='.$contestEntry['id'].'">Delete</a>';
											echo    '<span>'.$contestEntry['likes'].'</span>';
											echo '</li>';
										}
										echo '<li class="clear"></li>';
									echo '</ul>';
									
								?>
									
								<div class="refresh-container">
									<a href="<?php echo $journalAdminURL; ?>&updateContestEntry=1" class="button button-small button-primary">Refresh</a>
									<a href="<?php echo $journalAdminURL; ?>&deleteJournal=1" class="button button-small delete-journal">Delete All</a>
								
									<small>
										Note: Due to API limitations, this can only be used on 5000 photos per hour before you reach 
										your request limit. For more information, see the 
										<a target="_blank" href="https://instagram.com/developer/limits/">Instagram Endpoints</a>.
									</small>
								</div>	
									
								<?php
									
								} else { 									
									echo '<ul>';
										echo '<li class="clear"></li>';
									echo '</ul>';
								} 
							?>
						</div>
						
						<div class="clearfix"></div>	
					</div>
					
					<div class="rm_input rm_textarea rm_instagram_template">
						<label>Contest Gallery Mode:</label>
					
						<div class="radio-container">
							<input type="radio" name="isjournal_contest_gallery_mode"  value="classic" <?php if( $isjournalcontestgallerymode != 'contest' ){ echo "checked='checked'"; }?> />
							Classic View<br />
						
							<input type="radio" name="isjournal_contest_gallery_mode" value="contest" <?php if( $isjournalcontestgallerymode == 'contest' ){ echo "checked='checked'"; }?> />
							Contest View<br />
						</div>
					
						<small>
							This feature allows you to choose how you would like to render the photos.
						</small>
						<div class="clearfix"></div>	
					</div>
					
					<div class="rm_input rm_textarea rm_instagram_gallery_mode">
						<label>Get The Code</label>
						
						<div class="radio-container">
							Drop the following shortcode on your page to start the contest!<br />
						</div>
						
						<small class="green">
							[instagram-journal-contest]
						</small>
						<div class="clearfix"></div>	
					</div>
					
					<div class="rm_input rm_textarea rm_instagram_gallery_mode">
						<div class="radio-container text">
							You select photos simply by liking them from the Instagram App on you mobile device. 
							Any photo with the hashtag specified above will be in the gallery but they will not appear 
							until you approve them.
						</div>
						<div class="clearfix"></div>	
					</div>
					
				</div>
			</div>
						
			<input type="hidden" value="save" name="action">
		</form>
		
		<form method="post">
			<p class="submit">
				<input class="button" type="submit" value="Reset" name="reset">
				<input type="hidden" value="reset" name="action">
			</p>
		</form>
	</div> 
	<div class="clear"></div>
</div>
